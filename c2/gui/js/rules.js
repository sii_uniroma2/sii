
var statementRegex = /([a-z_]+[\.[a-z_]+]?)[ ]*(<|<=|==|>=|>|!=)[ ]*(.[^ ]+)/;

var rules = [];

var ruleList;

function addStatement(stmt)
{
	var macroStatements = [];

	var statementList = stmt.split("and");

	var statementGood = true;

	// for each statement:
	for (var k in statementList)
	{
		var x = statementList[k];

		var match = statementRegex.exec(x);

		// check statementRegex
		if (match)
		{
			var obj = { field: match[1], op: match[2], value: match[3] };

			macroStatements.push(obj);
		}
		else
		{
			console.log("Bad rule format: " + x);

			statementGood = false;

			alert("Error in statement, check syntax");

			break;
		}
	}

	if (statementGood)
	{
		rules.push( macroStatements );

		ruleList.add(stmt);
	}
}


function removeSelectedStatement()
{
	// call ruleList.removeSelected()
	// retreive index value of removed item
	// remove rule from [rules] at index

	var index = ruleList.removeSelected();

	if (index != -1)
	{
		rules.splice(index, 1);
	}
}


function sendRules()
{
	commandManager.send("rules-set", selectedSensor.sensorId, rules);
}


function statementToString(stmt)
{
	return stmt.field + " " + stmt.op + " " + stmt.value;
}


function createRulesfromObject(raw)
{
	ruleList.clearAll();

	rules = []

	for (var k in raw)
	{
		var rule = raw[k];

		var statementsNum = rule.length;

		if (statementsNum > 0)
		{
			var stmt_s = statementToString(rule[0]);

			for (var i = 1; i < statementsNum; i++)
			{
				stmt_s += " and " + statementToString(rule[i]);
			}

			// console.log("createRule: " + stmt_s);

			addStatement(stmt_s);
		}
	}
}

/*
function rulesToJson()
{
	var obj = { data: rules };

	var json = JSON.stringify(obj);

	console.log(json);

	return json;
}
*/
