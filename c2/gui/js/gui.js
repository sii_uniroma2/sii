
// DECLARING VARIABLES

var ws;

var commandManager;
var dataTable;
var sensorTable;
var sensorTableColumns;

var selectedSensor;


// DECLARING FUNCTIONS

function initTables()
{
	var dataTableColumns = [
		{ text: 'Timestamp', datafield: 'Timestamp' },
		{ text: 'Source IP', datafield: 'Source' },
		{ text: 'Destination IP', datafield: 'Destination' },
		// { text: 'Source Port', datafield: 'Checksum' },
		// { text: 'Destination Port', datafield: 'Version' },
		{ text: 'Protocol', datafield: 'Protocol Id' },
		{ text: 'Length', datafield: 'Packet Length' }
	];

	sensorTableColumns = [
		{ text: "Sensor Id", datafield: "sensorId" },
		{ text: "Status", datafield: "state" },
		{ text: "Last Command", datafield: "command" },
		{ text: "Last Message", datafield: "message" }
	];

	dataTable = new SimpleDataTable('#jqxgrid', dataTableColumns, updateDetailPane);

	dataTable.enablePaging(20);
}

function refreshDataTable()
{
	dataTable.refresh();

	dataTable.timeout = null;
}


function refreshSensorTable()
{
	sensorTable.refresh();

	sensorTable.timeout = null;

	// update selectedRow

	selectedSensor = sensorTable.getSelectedRow();

	switchButtons();
}


function startWebSocket()
{
	if (!ws)
	{
		ws = new WebSocket("ws://localhost:9002");

		commandManager = new Commands(ws);

		ws.onerror = function()
		{
			console.log("WebSocket error");

			delete ws;

			ws = null;
		}
		
		ws.onopen = function()
		{
			// ws.send(JSON.stringify({"command": "start", "args": ""}));
		};

		ws.onmessage = function(evt)
		{

			// console.log(evt.data);

			var row = jQuery.parseJSON(evt.data);

			// console.log("Received message: " + evt.data);

			// PROTOTYPE FOR COMMAND RESPONSE

			// (sensorId, command, status, message)

			if (row.command)
			{
				sensorTable.appendUnique(row.sensorId, row);

				if (!sensorTable.timeout)
				{
					sensorTable.timeout = setTimeout(refreshSensorTable, 200);
				}

				if (row.command == "RULES GET")
				{
					createRulesfromObject(row.args);
				}
			}
			else
			{
				dataTable.append(row);

				if (!dataTable.timeout)
				{
					dataTable.timeout = setTimeout(refreshDataTable, 200);
				}
			}
		};
	}
};


function switchButtons()
{
	var sensorButton = document.getElementById("theButton");

	if (selectedSensor)
	{
		var row = selectedSensor;

		var state = row.state.toLowerCase();

		// console.log("updateSensorControls state -> " + state);

		if (state == "stopped")
		{
			sensorButton.innerHTML = "Start";

			// console.log("was stopped");
		}
		else
		if (state == "running")
		{
			sensorButton.innerHTML = "Stop";
		
			// console.log("was running");
		}

		sensorButton.disabled = false;
	}
}

function updateSensorControls(evt)
{
	var row = sensorTable.getRowByIndex(evt.args.rowindex);
 
	selectedSensor = row;

	switchButtons();
}


function onControlPressed(btn)
{
	btn.disabled = true;

	btn.innerHTML = "Sending...";

	var state = selectedSensor.state.toLowerCase();

	if (state == "stopped")
	{
		commandManager.send("start", selectedSensor.sensorId);
	}
	else
	if (state == "running")
	{
		commandManager.send("stop", selectedSensor.sensorId);
	}

	// $("#jqxgrid").jqxGrid('unselectrow', selectedIndex);
}

function updateDetailPane(evt)
{
	var row = dataTable.getRowByIndex(evt.args.rowindex);

	var ddata = [];

	var ip = { label: "Internet Protocol", expanded: true, items : [] };

	var proto = row["Protocol Id"];

	var payload = row.Payload;
	var registers = row.Registers;

	// delete row.Payload;

	var additional = { label: "", expanded: true, items : [] };


	for (var key in row)
	{
		var obj = { label : key + ":" + row[key] };

		ip.items.push(obj);
	}

	ip.items.pop(); // pop registers
	ip.items.pop(); // pop payload

	for (var key in payload)
	{
		var obj = { label : key + ":" + payload[key] };

		additional.items.push(obj);
	}


	if (proto == "17")
	{
		additional.label = "UDP Payload";
	}
	else
	if (proto == "6")
	{
		additional.label = "TCP Payload";
	}


	ddata[0] = ip;

	if (additional.items.length > 0)
	{
		ddata[1] = additional;
	}


	var index = 2;

	for (var k in registers)
	{
		var plugin = registers[k];

		for (var key in plugin)
		{
			regItems = [];

			var values = plugin[key];

			for (var field in values)
			{
				regItems.push( { label: field + ": " + values[field] } );
			}

			ddata[index] = { label: key, expanded: true, items: regItems };

			index += 1;
		}
	}

	// console.log(out);

	$("#jqxTree").jqxTree(
	{
		source : ddata,
		width  : "100%"
	});
}

function bin2str_nullTerm(array) {
  var result = "";
  for (var i = 0; i < array.byteLength && array[i]!=0; i++) {
    result += String.fromCharCode(array[i]);
  }
  return result;
}

