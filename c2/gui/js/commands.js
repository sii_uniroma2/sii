

function Commands(webSock)
{
	this.ws = webSock;

	this.commandMap = {

		"start"      : { "command" : "start"      },
		"stop"       : { "command" : "stop"       },
		"status"     : { "command" : "status"     },
		"rules-set"  : { "command" : "rules-set"  },
		"rules-get"  : { "command" : "rules-get"  },

		"status-all" : { "command" : "status-all" }
	};

};

Commands.prototype.send = function(cmd, sensorId, args)
{
	var commandObj = this.commandMap[cmd];

	if (commandObj)
	{
		commandObj["sensorId"] = sensorId;
		commandObj["args"]     = args;

		commandStr = JSON.stringify(commandObj);

		this.ws.send( commandStr );
	}
};
