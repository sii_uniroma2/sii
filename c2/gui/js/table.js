
/* The SimpleDataTable class will wrap
   a jqxTable object */

function SimpleDataTable(elemId, columnList, onSelect)
{
	this.tableObj = $(elemId);
	this.data = [];
	this.uniqueRecords = [];

	this.source = { source: "array", localdata: this.data };

	this.dataAdapter = new $.jqx.dataAdapter(this.source);

	this.tableObj.jqxGrid(
	{
		source: this.dataAdapter,
		width: "100%",
		height: "100%",
		columnsresize: true,
		columns: columnList
	});

	if (onSelect)
	{
		this.tableObj.on('rowclick', onSelect);
	}
};


SimpleDataTable.prototype.refresh = function()
{
	var i = 0;

	for (var rid in this.uniqueRecords)
	{
		this.data[i] = this.uniqueRecords[rid];

		i += 1;
	}

	this.tableObj.jqxGrid('refreshdata');
};


SimpleDataTable.prototype.append = function(row)
{
	this.data.push(row);
};


SimpleDataTable.prototype.appendUnique = function(id, row)
{
	this.uniqueRecords[id] = row;
};


SimpleDataTable.prototype.clear = function()
{
	this.tableObj.jqxGrid('clear');
}


SimpleDataTable.prototype.enablePaging = function(pages)
{
	if (!pages) pages = 20;

	this.tableObj.jqxGrid({pageable: true, pagesize: pages});
}


SimpleDataTable.prototype.getRowByIndex = function(rowindex)
{
	return this.tableObj.jqxGrid('getrowdata', rowindex);
}


SimpleDataTable.prototype.getSelectedRow = function()
{
	var index = this.tableObj.jqxGrid('getselectedrowindex');

	return this.getRowByIndex(index);
}
