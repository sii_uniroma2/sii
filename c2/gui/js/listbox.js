

var ListBox = function(elemId)
{
	this.list = $(elemId);
	// this.data = [];

	this.list.jqxListBox( {width: "100%", height: "300px"} );
};


ListBox.prototype.add = function(entry)
{
	this.list.jqxListBox('addItem', entry);
};


ListBox.prototype.removeSelected = function()
{
	var index = this.list.jqxListBox('getSelectedIndex');

	if (index != -1)
	{
		this.list.jqxListBox('removeAt', index);
	}

	return index;
};


ListBox.prototype.clearAll = function()
{
	this.list.jqxListBox('clear');
};
