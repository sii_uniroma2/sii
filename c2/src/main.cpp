

#include <iostream>
#include <stdexcept>
#include <SSL_Context.hpp>
#include <SSL_Gate.hpp>
#include <SSL_Channel.hpp>
#include <Protocols.hpp>
#include <WebSocket.hpp>
#include <CommandProtocol.hpp>
#include <thread>
#include <SensorHandler.hpp>
#include <ConnectionListener.hpp>
#include <rapidjson/document.h>
#include <GUI_Callbacks.hpp>
#include <Config.hpp>

using namespace ssi;

// Parse Config

Config cfg = Config::parse("default.cfg");

// SETUP GUI
WebSocketServer gui(cfg.WebPort);



void __publish_handler(struct packet_data * data, void* args)
{
	assert(args);

	struct publishing_args * p_args = reinterpret_cast<struct publishing_args *>(args);
	
	gui.send((data)->toJson(*(p_args->registerJson), p_args->sensor_id ));
}

void server_listener()
{
	WebSocketServer::onMessageReceive(webgui::webgui_callback);

	// START GUI CONNECTION LISTNER
	gui.start();

	DataPublisher::addHandler(__publish_handler);

	ConnectionListener& listener = ConnectionListener::getInstance();
	
	listener.startListening(cfg.ListenAddress.c_str(), cfg.ListenPort);
	// sleep(1);
	listener.join();
}


int main()
{
	std::cout << "C2 Hello World!\n";

	SSL_Context& secure = SSL_Context::getInstance();

	secure.setCertificateAutority(cfg.CertAuth);
	secure.setCertificateKeyPair(cfg.CertPath, cfg.CertKey);

	std::thread srv( server_listener );

	srv.join();

	return 0;
}
