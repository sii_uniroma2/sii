
#include <GUI_Callbacks.hpp>
#include <RuleContainer.hpp>

using namespace ssi;

extern WebSocketServer gui;

		void webgui::webgui_callback(const std::string& message)
		{
			std::cout << "[GUI CMD RECEIVED] " << message << std::endl;

			// vars

			ConnectionListener& listener = ConnectionListener::getInstance();

			char responseBuffer[RESPONSE_BUFFER_SIZE];

			sensor_default_reply* response = reinterpret_cast<sensor_default_reply*>(responseBuffer);

			rapidjson::Document cmd;

			uint64_t sensorId = 0;
			command_id commandId = command_id::UNKNOWN;
			void* commandArgs = NULL;

			// RuleContainer* rc = NULL;

			std::string received;
			std::string moreJson = "[]";


			// code

			cmd.Parse<0>(message.c_str());

			const rapidjson::Value& argsValue = cmd["args"];

			received = cmd["command"].GetString();

			if (cmd.HasMember("sensorId"))
			{
				std::string sensorString = cmd["sensorId"].GetString();

				sensorId = strtoull(sensorString.c_str(), NULL, 10);
			}

	
			if (received == "start")
			{
				commandId = command_id::START;
			}
			else if (received == "stop")
			{
				commandId = command_id::STOP;
			}
			else if (received == "pause")
			{
				commandId = command_id::PAUSE;
			}
			else if (received == "status-all")
			{
				std::vector<std::string> replies;

				guicommand_statusAll( replies );

				for (auto& reply : replies)
				{
					gui.send(reply);
				}

				return;
			}
			else if (received == "rules-set")
			{
				// const rapidjson::Value& args = cmd["args"];

				commandId = command_id::RULES_SET;
				commandArgs = (void*) &argsValue;

				/*
				rc = new RuleContainer();

				if ( RuleContainer::fromJsonArray(args, rc) )
				{
					commandId = command_id::RULES_SET;
					commandArgs = rc;
				}
				*/

				// answer = guicommand_rules_set(sensorId, args);
			}
			else if (received == "rules-get")
			{
				commandId = command_id::RULES_GET;

				commandArgs = &moreJson;
			}


			// DISPATCH COMMAND

			int responseBytes = listener.dispatchCommand(sensorId, commandId, response, commandArgs);
		
			// NOTIFY GUI
	
			gui.send( response->toJson(sensorId, moreJson) );

			// DO SOME CLEANUP IF NEEDED

			/*
			if (rc != NULL)
			{
				delete rc;
			}
			*/
		}

/*
		std::string webgui::handle_response(uint64_t sensorId, const sensor_default_reply* response, int responseBytes)
		{
			std::string moreJson = "[]";

			std::cout << "[HANDLE RESPONSE] " << response->toJson(sensorId) << "\n";

			if (responseBytes > 0 && response->reply == OK) // valid response
			{
				if (response->commandId == RULES_GET)
				{
					RuleContainer receivedRules;

					if ( RuleContainer::fromBuffer(response->args, responseBytes-sizeof(sensor_default_reply), &receivedRules) )
					{
						std::cout << "[webgui::handle_response] Binary rules: \n";

						receivedRules.print();

						// convert to json

						moreJson = receivedRules.toJson();

						std::cout << "[webgui::handle_response] Json rules: \n" << moreJson << "\n";
					}
				}
			}

			return response->toJson(sensorId, moreJson);
		}
*/

		/*		
		std::string webgui::guicommand_start(uint64_t sensor_id)
		{
			ConnectionListener& listener = ConnectionListener::getInstance();
			struct sensor_default_reply reply;
			if(listener.dispatchCommand(sensor_id, command_id::START, &reply)<0)
			{
				std::cout << "[GUI Command START] Dispatch command failed.\n";
			}
			
			return reply.toJson(sensor_id);
		}
		
		std::string webgui::guicommand_stop(uint64_t sensor_id)
		{
			ConnectionListener&  listener = ConnectionListener::getInstance();

			struct sensor_default_reply reply;

			if(listener.dispatchCommand(sensor_id, command_id::STOP, &reply)<0)
			{
				std::cout << "[GUI Command STOP] Dispatch command failed.\n";
			}

			return reply.toJson(sensor_id);
		}
		
		std::string webgui::guicommand_pause(uint64_t sensor_id)
		{
			ConnectionListener&  listener = ConnectionListener::getInstance();

			struct sensor_default_reply reply;

			if (listener.dispatchCommand(sensor_id, command_id::PAUSE, &reply) < 0)
			{
				std::cout << "[GUI Command PAUSE] Dispatch command failed.\n";
			}

			return reply.toJson(sensor_id);
		}
		
		std::string webgui::guicommand_rules_set(uint64_t sensor_id, const rapidjson::Value& args)
		{
			ConnectionListener& listener = ConnectionListener::getInstance();

			struct sensor_default_reply reply;

			RuleContainer rc;

			if ( RuleContainer::fromJsonArray(args, &rc) )
			{
				// pass rc to dispatcher

				if ( listener.dispatchCommand(sensor_id, command_id::RULES_SET, &reply, &rc) < 0 )
				{
					std::cout << "[GUI Command RULES_SET] Dispatch command failed.\n";
				}
				
			}

			return reply.toJson(sensor_id);
		}

		std::string webgui::guicommand_rules_get(uint64_t sensor_id)
		{
			ConnectionListener& listener = ConnectionListener::getInstance();

			char commandBuffer[1024];

			if (listener.dispatchCommand(sensor_id, command_id::RULES_GET, (sensor_default_reply*) commandBuffer) < 0)
			{				
				std::cout << "[GUI Command RULES_SET] Dispatch command failed.\n";
			}

			return "";
		}
		
		std::string webgui::guicommand_status(uint64_t sensor_id)
		{
			return NULL;
		}
		*/

		void webgui::guicommand_statusAll( std::vector<std::string>& answers )
		{
			ConnectionListener& listener = ConnectionListener::getInstance();

			std::map<uint64_t, struct sensor_default_reply> replyMap;

			listener.queryAll(replyMap);

			answers.reserve( replyMap.size() );

			for (auto& entry : replyMap)
			{
				uint64_t sensorId = entry.first;

				struct sensor_default_reply& reply = entry.second;

				answers.push_back( reply.toJson(sensorId) );
			}
		}


