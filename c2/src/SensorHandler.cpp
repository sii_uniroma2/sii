
#include <SensorHandler.hpp>

#include <rapidjson/document.h>

namespace ssi
{

		std::default_random_engine SensorHandler::RAND_ENGINE(std::chrono::system_clock::now().time_since_epoch().count());
		
		SensorHandler::SensorHandler(SensorHandler_params * params)
		{
			this->channel = params->channel;
			this->sensorID = params->sensorID;
			
			this->data_transfer_handler = NULL;
		}

		/*
		 * This function will handle response, and in particular distinguish
         * between three cases:
		 *
         * a) RESPONSE is valid (it will forward as is to GUI)
         * b) RESPONSE is NOT valid
         * c) RESPONSE NOT received due to a COMMUNICATION ERROR
		 *
		 * Return values differs between the cases:
         *
         * a) return the number of bytes read
         * b) return 0
         * c) return -1
         *
         * So a value greater or equal to 0 means that communication link is good and
         * viceversa a value less than 0 means communcation error.
		 *
		 * In cases b) and c) the response is MODIFIED to update the FAIL REASON.
		 *
		 */
		int SensorHandler::waitResponse(command_id commandId, sensor_default_reply* response)
		{
			assert( response );

			// int retcode = 0;

			// TODO: we need to handle TIMEOUTS
			int readBytes = channel->recv_msg( (char*) response, RESPONSE_BUFFER_SIZE, 5 /* seconds timeout */ );

			if (readBytes < 0)
			{
				response->commandId    = commandId;
				response->reply        = return_code::COMM_ERROR;
				response->sensorStatus = 2; // UNKNOWN

				std::cerr << "[Command RESPONSE ERROR: IN READ] " << response->toJson(sensorID) << "\n\n";

				readBytes = -1;
			}
			else
			if (readBytes == 0)
			{
				response->commandId    = commandId;
				response->reply        = return_code::COMM_ERROR;
				response->sensorStatus = 0;

				std::cerr << "[Command RESPONSE ERROR: CLOSE] " << response->toJson(sensorID) << "\n\n";

				readBytes = -1;
			}
			else
			if ( readBytes < (int) sizeof(sensor_default_reply) || response->commandId != commandId)
			{
				response->commandId    = commandId;
				response->reply        = return_code::BAD_RESPONSE;
				response->sensorStatus = 2; // UNKNOWN

				std::cerr << "[Command RESPONSE ERROR: RESPONSE NOT VALID] " << response->toJson(sensorID) << "\n\n";

				readBytes = 0;
			}

			return readBytes; 
		}

		  
		int SensorHandler::dispatchCommand( command_id command,
											sensor_default_reply * reply,
											void* data,
											size_t dataLen)
		{
			int retcode = -1;

			if (channel)
			{
				switch(command)
				{
					case START:
						retcode = command_start(reply); break;
					case STOP:
						retcode = command_stop(reply); break;
					case PAUSE:
						retcode = command_pause(reply); break;
					case STATUS:
						retcode = command_status(reply); break;
					case RULES_GET:
						retcode = command_rules_get(reply, data); break;
					case RULES_SET:
						retcode = command_rules_set(reply, data, dataLen); break;
					case PLUGIN_GET:
						retcode = command_plugin_get(); break;
					default:
						std::cerr << "[SensorHandler::dispachCommand] Command NOT valid.\n\n";
						break;
				}
			}
			else
			{
				std::cerr << "[SensorHandler::dispachCommand] Channel destroyed.\n\n";
			}

			return retcode;
		}

		SSL_IChannel * SensorHandler::accept_dataTransfer(SSL_Gate * gate, int port)
		{
			fd_set rfds;
			struct timeval tv;
			int retval;

		   /* Watch stdin (fd 0) to see when it has input. */
			FD_ZERO(&rfds);
			FD_SET(port, &rfds);

		    retval = select(1, &rfds, NULL, NULL, &tv);
			/* Don't rely on the value of tv now! */

		   	if (retval == -1)
		   	{
				perror("select()");
				return NULL;
			}
			else if (retval)
			{
				printf("Sensor requested connection.\n");
				SSL_IChannel * channel = gate->accept_channel();
				return channel;
			}
				/* FD_ISSET(0, &rfds) will be true. */
			
			printf("No sensor request for connection in time.\n");
			
			return NULL;
		}
		
		int SensorHandler::bindPort(int sock)
		{
			struct sockaddr_in serv_address;
			in_addr_t net_addr;
			uint16_t port_number = 12355;
			
			if(inet_pton(AF_INET, "127.0.0.1", (void*)&net_addr)<0)
			{
				perror("something wrong with SensorHandler::bindPort()::inet_pton()");
				exit(-1);
			}
			
			serv_address.sin_family = AF_INET;
			serv_address.sin_port = port_number;
			serv_address.sin_addr.s_addr = net_addr;
			
			if(bind(sock, (struct sockaddr*)&serv_address, sizeof(sockaddr))<0)
			{
				perror("something wrong with SensorHandler::bindPort()::bind()");
			}
			
			return port_number;
		}
		
		int SensorHandler::generatePortNumber()
		{
			std::uniform_real_distribution<double> distr(49152,65536);
			
			return (uint16_t) distr(SensorHandler::RAND_ENGINE);
		}

		
		int SensorHandler::command_start(sensor_default_reply * response)
		{
			int retcode = 0;

			int port_number;
			
			struct timeval tv;
			tv.tv_sec = SensorHandler::MAX_TIMEOUT_SEC;
			tv.tv_usec = SensorHandler::MAX_TIMEOUT_MICROSEC;

			if (this->data_transfer_handler)
			{
				if (!this->data_transfer_handler->isRunning())
				{
					delete this->data_transfer_handler;

					this->data_transfer_handler = NULL;
				}
			}

			assert(this->data_transfer_handler == NULL);

			/*	
			if(this->data_transfer_handler!=NULL)
			{
				printf("sensor already transferring\n");
				return -1;
			}
			*/

			SSL_Gate gate(SSL_Context::getInstance().getRawContext());
			
			do
			{
				port_number = generatePortNumber();
				std::cout<< "trying port... " << port_number << "\n";
			}
			while(gate.set_gate("0.0.0.0", port_number)<0);
		
			struct sensor_start command;
			command.port = port_number; // htons((uint16_t) port_number);
			
			if(this->channel->send_msg((char*) &command, sizeof(sensor_start))<0)
			{
				std::cerr << "[SensorHandler] Error sending START command\n";
				return -1;
			}
			
			SSL_IChannel * data_channel = gate.accept_channel();
			
			if(data_channel == NULL)
			{
				std::cerr << "[SensorHandler] data_channel NULL after accept!\n";
				//printf("Timeout\n");
				return -1;
			}

			retcode = waitResponse(command_id::START, response);
			
			SensorDataHandler_params params;
			
			params.sensor_id = this->sensorID;
			params.channel = data_channel;
			params.pluginsMap = &pluginsMap;
			
			this->data_transfer_handler = new SensorDataHandler(params);
			
			this->data_transfer_handler->start();
			
			return retcode;
		}
		
		
		int SensorHandler::command_stop(sensor_default_reply * reply)
		{
			int retcode = 0;

			struct sensor_command command(STOP);

			channel->send_msg( (char*) &command, sizeof(command) );

			retcode = waitResponse(STOP, reply);

			this->data_transfer_handler->stop();

			std::cout << "[SensorHandler] Stop data handler done.\n";

			delete this->data_transfer_handler;

			this->data_transfer_handler = NULL;

			return retcode;
		}
		
		int SensorHandler::command_status(sensor_default_reply * reply)
		{
			struct sensor_command command(STATUS);

			// send command

			channel->send_msg( (char*) &command, sizeof(command) );

			return waitResponse(STATUS, reply);	
		}
		
		int SensorHandler::command_rules_set(sensor_default_reply * reply, const void* buffer, size_t bufferLen)
		{
			// const RuleContainer* rc = reinterpret_cast<const RuleContainer*>(buffer);

			const rapidjson::Value* jsonRules = reinterpret_cast<const rapidjson::Value*>(buffer);

			RuleContainer rc;

			if ( !RuleContainer::fromJsonArray(*jsonRules, &rc, &pluginsMap) )
			{
				// signal gui bad parsing!

				return 0;
			}

			char commandBuffer[1024];

			struct sensor_command* command = reinterpret_cast<struct sensor_command*>(commandBuffer);

			command->commandId = RULES_SET;

			int written = rc.toBuffer( command->args, 1024 - sizeof(sensor_command) );

			if (written == -1)
			{
				std::cerr << "[SensorHandler::command_rules_set] Error writing commandBuffer!\n\n";

				return 0;
			}

			// now send

			channel->send_msg( commandBuffer, sizeof(sensor_command) + written );

			return waitResponse(RULES_SET, reply);
		}

		
		int SensorHandler::command_rules_get(sensor_default_reply* response, void* data)
		{
			struct sensor_command command(RULES_GET);

			channel->send_msg( (char*) &command, sizeof(command) );

			int receivedBytes = waitResponse(RULES_GET, response);

			if (receivedBytes > 0 && response->reply == OK)
			{
				std::string& moreJson = *reinterpret_cast<std::string*>(data);

				RuleContainer receivedRules;

				if ( RuleContainer::fromBuffer(response->args, receivedBytes-sizeof(sensor_default_reply), &receivedRules) )
				{
					std::cout << "[webgui::handle_response] Binary rules: \n";

					receivedRules.print();

					// convert to json

					moreJson = std::move(receivedRules.toJson(&pluginsMap));

					std::cout << "[webgui::handle_response] Json rules: \n" << moreJson << "\n";
				}
			}
		}
		
		int SensorHandler::command_pause(sensor_default_reply * reply)
		{
			return -1;
		}


		int SensorHandler::command_plugin_get()
		{
			struct sensor_command command(PLUGIN_GET);

			char responseBuffer[RESPONSE_BUFFER_SIZE+1]; // +1 due to terminator

			sensor_default_reply* response = reinterpret_cast<sensor_default_reply*>(responseBuffer);

			channel->send_msg( (char*) &command, sizeof(command) );

			int receivedBytes = waitResponse(PLUGIN_GET, response);

			if (receivedBytes > 0 && response->reply == OK)
			{
				responseBuffer[receivedBytes] = '\0';

				std::cout << "PLUGINS: " << response->args << "\n";

				rapidjson::Document json;

				json.Parse<0>(response->args);

				std::cout << "[command_plugin_get] document elements: " << json.Size() << "\n";

				for (rapidjson::SizeType i = 0; i < json.Size(); i++)
				{
					const rapidjson::Value& val = json[i];

					std::string plugin_s = val["plugin"].GetString();

					const rapidjson::Value& fields = val["fields"];

					std::vector<std::string> fields_v( fields.Size() );

					for (rapidjson::SizeType j = 0; j < fields.Size(); j++)
					{
						fields_v[j] = fields[j].GetString();
					}

					// retreive plugin_id

					uint64_t plugin_id = strtoull(plugin_s.c_str(), NULL, 10);

					// finally commit to map

					pluginsMap[plugin_id] = fields_v;
				}


				for (auto& x : pluginsMap)
				{
					std::cout << "plugin: " << x.first << "\n";
					
					for (auto& y : x.second)
					{
						std::cout << "\tfield: " << y << "\n";
					}
				}
			}


			return receivedBytes;
		}
}
