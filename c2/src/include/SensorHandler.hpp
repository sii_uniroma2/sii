
#ifndef SENSORHANDLER_HPP_
#define SENSORHANDLER_HPP_

#include <common.hpp>
#include <DLeft.cpp>
#include <HashedBuffer.cpp>
#include <SSL_IChannel.hpp>
#include <SSL_Channel.hpp>
#include <SSL_Gate.hpp>
#include <SSL_Context.hpp>
#include <SensorDataHandler.hpp>
#include <Thread.hpp>
#include <Protocols.hpp>
#include <CommandProtocol.hpp>
#include <random>
#include <chrono>
#include <map>
#include <RuleContainer.hpp>

/* According to POSIX.1-2001 */
#include <sys/select.h>


namespace ssi
{
	
	struct SensorHandler_params
	{
		SSL_IChannel * channel;
		uint64_t sensorID;
	};
	
	class SensorHandler 
	{
		
		private:
		
		static const uint32_t MAX_BUFFER_SIZE = 3000;
		static const uint16_t MAX_TIMEOUT_SEC = 5;
		static const uint32_t MAX_TIMEOUT_MICROSEC = 0;
		
		static std::default_random_engine RAND_ENGINE;
		
		SSL_IChannel * channel;
		uint64_t  sensorID;
		SensorDataHandler * data_transfer_handler;

		std::map<uint64_t, StringList> pluginsMap;
		
		protected:
		
		SSL_IChannel * accept_dataTransfer(SSL_Gate * gate, int port);
		int bindPort(int sock);

		int waitResponse(command_id commandId, sensor_default_reply* response);

		public:
		
		SensorHandler(SensorHandler_params * params);
		
		int generatePortNumber();
		int dispatchCommand(command_id commandId,
							sensor_default_reply * reply,
							void* data = NULL,
							size_t dataLen = 0 );
		
		int command_start(sensor_default_reply *);
		int command_stop(sensor_default_reply *);
		int command_status(sensor_default_reply *);
		int command_rules_get(sensor_default_reply *, void* data);
		int command_rules_set(sensor_default_reply *, const void* data, size_t dataLen);
		int command_pause(sensor_default_reply *);
		int command_plugin_get();


		virtual ~SensorHandler()
		{
			if (data_transfer_handler)
			{
				delete data_transfer_handler;
			}
		}
	};
	
	
}

#endif
