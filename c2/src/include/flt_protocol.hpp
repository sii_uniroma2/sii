/*
 * flt_protocol.hpp
 *
 *  Created on: Jan 3, 2013
 *      Author: gabriele
 */

#ifndef FLT_PROTOCOL_HPP_
#define FLT_PROTOCOL_HPP_


typedef enum : unsigned short
{
	FLT_PROTO_UPD,	// update rules
	FLT_PROTO_NFY,	// notify rules
	FLT_PROTO_CLR 	// clear rules
}	flt_op_t;

typedef struct
{
	flt_op_t op;
	unsigned long data_size;

} flt_cmd_t;

#endif /* FLT_PROTOCOL_HPP_ */
