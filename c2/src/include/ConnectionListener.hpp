/*
 * ConnectionListener.hpp
 *
 *  Created on: Mar 28, 2013
 *      Author: gabriele
 */

#ifndef CONNECTIONLISTENER_HPP_
#define CONNECTIONLISTENER_HPP_


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <cstdlib>

#include <common.hpp>

#include <SensorHandler.hpp>
#include <SSL_Context.hpp>
#include <SSL_IChannel.hpp>
#include <SSL_Gate.hpp>
#include <Thread.hpp>
#include <vector>
#include <map>

namespace ssi {


	class ConnectionListener : public ssi::Thread{

	private:

		SSL_CTX * ssl_context;
		SSL_Gate * ssl_gate_listener;

		// vector dei sensori attivati
		
		std::map<uint64_t, SensorHandler*> active_sensors;
		
		int set_gate(const char * ip_interface, const int port);


		ConnectionListener();
		
	public :

		
		ConnectionListener(const ConnectionListener&) = delete;
		
		virtual ~ConnectionListener()
		{
			if(ssl_gate_listener!=NULL)
				delete ssl_gate_listener;
		}
		
		static ConnectionListener& getInstance()
		{
			static ConnectionListener instance;
			return instance;
		}

		void removeSensors( const std::vector< std::pair<uint64_t, SensorHandler*> >& sensorList )
		{
			for (auto& sensor : sensorList)
			{
				delete sensor.second; // destroying SensorHandler

				active_sensors.erase(sensor.first); // removing from ActiveSensors 
			}
		}

		int setConnectionInterface(const char * ip_interface = "127.0.0.1", const int port = DEF_SERVER_PORT);

		int executeCommand(int sensorId, sensor_command * command, sensor_command * reply);
	
		int startListening(const char * ip_interface, const int port);

		int startListening();

		int stopListening();
	
		SensorHandler * getSensorHandler(uint64_t sensor_id);

		int dispatchCommand(uint64_t sensorId,
							command_id commandId, 
							struct sensor_default_reply* reply,
							void* data = NULL,
							size_t dataLen = 0);

		int queryAll( std::map<uint64_t, struct sensor_default_reply>& replyMap );

	protected:


		uint64_t generateSensorID(const struct sockaddr_in *address);
	
		void run();


	};

}
#endif /* CONNECTIONLISTENER_HPP_ */



