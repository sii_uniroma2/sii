
#pragma once

#include <ConnectionListener.hpp>
#include <rapidjson/document.h>

#include <string>

#include <Protocols.hpp>
#include <WebSocket.hpp>
//#include <extern_gui.hpp>


namespace ssi
{
	namespace webgui
	{
		std::string guicommand_start(uint64_t sensor_id);
		std::string guicommand_stop(uint64_t sensor_id);
		std::string guicommand_status(uint64_t sensor_id);
		std::string guicommand_pause(uint64_t sensor_id);
		std::string guicommand_rules_set(uint64_t sensor_id, const rapidjson::Value& args);

		void guicommand_statusAll( std::vector<std::string>& replies );
		
		void webgui_callback(const std::string& message);

		std::string handle_response(uint64_t sensor_id, const sensor_default_reply* response, int argsSize);
	}
}
