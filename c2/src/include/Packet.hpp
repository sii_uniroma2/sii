
#pragma once

#include <Protocols.hpp>
#include <Registers.hpp>
#include <iostream>
#include <vector>
#include <stdexcept>
#include <stdint.h>
#include <memory>

namespace ssi
{
  class Packet 
  {
    // friend class CounterPlugin;

  protected:

    static const uint32_t MAX_ETH_SIZE = 1518;
    static const uint32_t MAX_IP_SIZE = 1500;

    size_t length;

    mutable Registers regs;

    uint8_t buffer[MAX_IP_SIZE];

    struct ipv4* ip_packet;

	mutable bool headParsed;

  public:
 
    enum fields
    {
        IP_PROTO_R,
        IP_SRC_R,
        IP_DST_R,
        SRC_PORT_R,
        DST_PORT_R,
        FLAGS_R        
    };

    static const uint32_t IP_OFFSET = 14;
    static const uint32_t IP_HEADER_LEN = 20;
    static const uint32_t TCP_HEADER_LEN = 40;

	const uint64_t timestamp_us;

    static const std::string ipToString(uint32_t ip);

 
    Packet(const uint8_t* _buffer, size_t _size, uint64_t _timestamp_us) : headParsed(false),
																		   timestamp_us(_timestamp_us)
	{
        if (_size < IP_OFFSET+4 || _size > MAX_ETH_SIZE )
        {
            char err[64];
            snprintf(err, 64, "Packet(): size error => %u\n", _size);

            throw std::runtime_error(err);
        }

        length = _size-IP_OFFSET-4; // Payload length

        memcpy(buffer, _buffer+IP_OFFSET, length);  // do not copy ethernet frame header
    };


    struct tcp* tcp() const
    {
        struct tcp* tcp_packet = NULL;

        if (ip_packet->protocol == 6 && length > sizeof(struct tcp))
        {
            tcp_packet = (struct tcp*) buffer;
			
			if (!headParsed)
			{
				uint32_t flags = ntohs(tcp_packet->offset_flags) & 0x000F;

            	regs[SRC_PORT_R] << ntohs(tcp_packet->source_port);
				regs[DST_PORT_R] << ntohs(tcp_packet->dest_port);

				regs[FLAGS_R] << flags; // tcp_packet->flags.value;

				headParsed = true;
			}
        }

        return tcp_packet;
    }


    struct udp* udp() const
    {
        struct udp* udp_packet = NULL;

        if (ip_packet->protocol == 17 && length > sizeof(struct udp))
        {
            udp_packet = (struct udp*) buffer;

			if (!headParsed)
			{
            	regs[SRC_PORT_R] << ntohs(udp_packet->source_port);
				regs[DST_PORT_R] << ntohs(udp_packet->dest_port);

				headParsed = true;
			}
        }

        return udp_packet;
    }


    Registers* getRegisters() const { return &regs; }


	std::shared_ptr<struct packet_buffer> getPacketData(bool withPayload = false) const;


    void parse();

  };

}




