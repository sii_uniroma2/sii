
#pragma once

#include <string>
#include <rapidjson/document.h>
#include <ostream>

class Config
{

	Config(const rapidjson::Document& doc) : ListenAddress( doc["ListenAddress"].GetString() ),
											 ListenPort( doc["ListenPort"].GetUint() ),
											 WebPort( doc["WebPort"].GetUint() ),
											 CertAuth( doc["CertAuth"].GetString() ),
											 CertKey( doc["CertKey"].GetString() ),
											 CertPath( doc["CertPath"].GetString() )
	{
	}

public:

	const std::string ListenAddress;
	const uint16_t    ListenPort;
	const uint16_t    WebPort;
	const std::string CertAuth;
	const std::string CertKey;
	const std::string CertPath;
	

	static Config parse(const std::string& path);


	friend std::ostream& operator<<(std::ostream& stream, const Config& cfg)
	{
		stream << "ListenAddress: " << cfg.ListenAddress << "\n";
		stream << "ListenPort: " << cfg.ListenPort << "\n";
		stream << "WebPort: " << cfg.WebPort << "\n";
		stream << "CertAuth: " << cfg.CertAuth << "\n";
		stream << "CertKey: " << cfg.CertKey << "\n";
		stream << "CertPath: " << cfg.CertPath << "\n";

		return stream;
	}
};
