/*
 * SensorHandler.hpp
 *
 *  Created on: Mar 28, 2013
 *      Author: gabriele
 */

#ifndef SENSORDATAHANDLER_HPP_
#define SENSORDATAHANDLER_HPP_

#include <common.hpp>
#include <DLeft.cpp>
#include <HashedBuffer.cpp>
#include <FHashedBuffer.hpp>
#include <SSL_IChannel.hpp>
#include <Thread.hpp>
#include <Protocols.hpp>
#include <DataPublisher.hpp>
#include <atomic>
#include <map>
#include <vector>
#include <string>
#include <mutex>

namespace ssi
{
	struct SensorDataHandler_params
	{
		SSL_IChannel * channel;
		uint64_t sensor_id;
		std::map<uint64_t, std::vector<std::string>>* pluginsMap;
	};

	struct dleft_table_entry
	{
		uint64_t sensor_id;
		uint64_t timestamp;
	};
	
	struct publishing_args
	{
		uint64_t sensor_id;
		std::string * registerJson;
	};

	class SensorDataHandler : public ssi::Thread {
	
	private:

		/*
		 *  	BUFFER_SIZE: maximum length of data received;
		 *	OLD_DATA_TIMEOUT: time to entry obsolescence;
		 *	KEY_BYTE_SIZE: maximum dimension of key used for dleft_table_entry insert/read; 	
		 */
		
		static const int BUFFER_SIZE = 3000;
		static const uint64_t OLD_DATA_TIMEOUT = 500000;
		static const uint32_t KEY_BYTE_SIZE = 128;
		
		static DLeft<struct dleft_table_entry> table;
		
		static std::mutex tableMutex;
		
		SSL_IChannel * channel;
		uint64_t sensor_id;
		std::map<uint64_t, std::vector<std::string>>* pluginsMap;

		std::atomic<bool> running;

	protected:

		void run();

		bool isPreviousDataOld(struct dleft_table_entry * prev, struct dleft_table_entry * next);

		// void notifyNewPacket(struct packet_data * data);

		int buildTableKey(struct ipv4 * data, HashedBuffer<KEY_BYTE_SIZE>& key);
		
		int buildTableKey(struct tcp * data, HashedBuffer<KEY_BYTE_SIZE>& key);
		
		int buildTableKey(struct udp * data, HashedBuffer<KEY_BYTE_SIZE>& key);

		// int parsePacket(char * buffer, const int rcv_size, struct packet_data * structured_packet);

		int sendCommand(char * buffer, int length);

		std::string createRegistersJson(const RegisterType* regs, size_t regsNumber);
		
	public:

		SensorDataHandler(const struct SensorDataHandler_params& params) : running(false)
		{
			this->channel = params.channel;
			this->sensor_id = params.sensor_id;
			this->pluginsMap = params.pluginsMap;
		}

		void stop()
		{
			running = false;

			this->join();
		}
	};

}

#endif /* SENSORHANDLER_HPP_ */
