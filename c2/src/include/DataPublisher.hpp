
#pragma once

#include <vector>
#include <cstdlib>

namespace ssi
{

	typedef void (* __data_output_handler)(struct packet_data *, void*);
	
	class DataPublisher {

	private :
	 	
	 	static std::vector<__data_output_handler> data_handlers;
	
	public:
	
		static int publish(struct packet_data * data, void* args)
		{
			for(auto handler : data_handlers)
			{
				handler(data, args);
			}
			return 0;
		}
		
		static int addHandler(__data_output_handler handler)
		{
			if(handler == NULL) return -1;
			
			data_handlers.push_back(handler);
			return 0;
		}

	};
}
