/*
 * SensorHandler.cpp
 *
 *  Created on: Mar 2, 2013
 *      Author: gabriele
 */

#include <SensorDataHandler.hpp>
#include <Registers.hpp>
#include <bsd/string.h>

namespace ssi
{
		DLeft<struct dleft_table_entry> SensorDataHandler::table(8,20);
		
		std::mutex SensorDataHandler::tableMutex;

			
		
		void SensorDataHandler::run()
		{
			int rcv=0;
			
			char buffer[BUFFER_SIZE];
			struct packet_data * received_packet_data;
			
			struct dleft_table_entry * table_entry;
			
			struct publishing_args p_args;
			p_args.sensor_id = this->sensor_id;
			
			FHashedBuffer key;

			size_t dataLength = 0;

			// running = true;
			
			while( (rcv = this->channel->recv_msg(buffer, BUFFER_SIZE)) > 0 )
			{
				std::string registerJson = "[]";
				
				
				// first receive registers (if needed)
				size_t regsNumber = *buffer;

				if (regsNumber > 0)
				{
					RegisterType regs[regsNumber];

					uint32_t offset = 1;

					for (size_t i = 0; i < regsNumber; i++)
					{
						offset += regs[i].deserialize(buffer + offset, BUFFER_SIZE - offset);
					}

					registerJson = createRegistersJson(regs, regsNumber);
				}

				// std::cerr << "[SensorDataHandler] Receieved " << rcv << " bytes of registers.\n";

				// now handling packet data
				rcv = this->channel->recv_msg(buffer, BUFFER_SIZE);

				if (rcv <= sizeof(packet_data_header)) continue;

				// std::cerr << "[SensorDataHandler] Receieved " << rcv << " bytes of data.\n";

				dataLength = rcv - sizeof(packet_data_header);

				received_packet_data = (struct packet_data *) buffer;
				
				/*
				if(parsePacket((char *)buffer, rcv, received_packet_data)<0)
				{
					printf("something wrong with SensorDataHandler::run()::parsePacket()\n");
					exit(-1);
				}
				*/
					
				key.setFKey(received_packet_data->head.hash);
				
				
				{
					std::lock_guard<std::mutex> lock(tableMutex);
					
					// table_entry = table.get(key);
					table_entry = this->table.reserve(key, false);
				
					// if(table_entry == NULL	// new_data_entry is First seen
					if ( table_entry->sensor_id ==  this->sensor_id // retransmission, not duplicate
						|| (received_packet_data->head.timestamp > table_entry->timestamp + OLD_DATA_TIMEOUT)  // previous entry too old 		
						)
					{
						// new packet for sure
						// table_entry = this->table.reserve(key, false);
						table_entry->sensor_id = this->sensor_id;
						table_entry->timestamp = received_packet_data->head.timestamp;
						
						p_args.registerJson = &registerJson;
						DataPublisher::publish(received_packet_data, &p_args);
						p_args.registerJson = NULL;
					}
					else
					{
						std::cerr << "Obsolete:\n";
						std::cerr << "onSensor:    " << this->sensor_id <<"\n";
						std::cerr << "(vs tSensor):" << table_entry->sensor_id << "\n";
						std::cerr << "time:        " << received_packet_data->head.timestamp << "\n";
						std::cerr << "timeOnTable: " << table_entry->timestamp << "\n";
						std::cerr << "expected_T:  " << (table_entry->timestamp + OLD_DATA_TIMEOUT) << " or >\n";
						std::cerr << "hash:        " ;
						for(int i = 0; i<6; i++)
						{
							printf("%08x ", received_packet_data->head.hash[i]);
						}
						std::cerr << "\n\n" ;
						
						
					}
				}
			}

			std::cerr << "[SensorDataHandler] Quit receiving cycle.\n";

			if(rcv<0)
			{
				perror("something wrong with SensorDataHandler::run()::channel->recv_msg()");
			}
		}


		std::string SensorDataHandler::createRegistersJson(const RegisterType* regs, size_t regsNumber)
		{
			static const char* __key_value_pair = "\"%s\":\"%s\",";

			char jsonBuffer[1024];
			uint32_t writtenJson = 1;

			jsonBuffer[0] = '[';

			int plugin = 0;

			auto& pluginsMap = *(this->pluginsMap);

			for (auto& pi : pluginsMap)
			{
				char buffer[1024];
				uint32_t writtenBytes = 2;

				const char* plugin_name = (const char*) &pi.first;

				buffer[0] = '{';
				buffer[1] = '\"';

				writtenBytes += strlcpy(buffer+writtenBytes, plugin_name, sizeof(uint64_t)); // pi.first is a uint64_t

				strcpy(buffer+writtenBytes, "\":{");

				writtenBytes += 3;

				auto& fieldList = pi.second;

				size_t offset;

				for (offset=0; offset < fieldList.size(); offset++)
				{
					auto& reg = regs[plugin + offset];

					// std::cerr << "[SensorDataHandler::run] Trying get value for reg@" << plugin+offset << "\n";

					// reg.dump();

					std::string value = reg.GetString();

					if (value == "") value = "n/a";

					writtenBytes += snprintf(buffer+writtenBytes,
											 1024-writtenBytes,
											 __key_value_pair,
											 fieldList[offset].c_str(),
											 value.c_str()
											);
				}

				// replace comma with right curly bracket
				buffer[writtenBytes-1] = '}';
				buffer[writtenBytes]   = '}';
				buffer[writtenBytes+1] = ',';
				buffer[writtenBytes+2] = '\0';

				// std::cerr << "Buffer: " << buffer << "\n";

				writtenJson += strlcpy(jsonBuffer + writtenJson, buffer, writtenBytes+3);

				plugin += offset;
			}

			// replace comma with square bracket
			jsonBuffer[writtenJson-1] = ']';
			jsonBuffer[writtenJson]   = '\0';

			// std::cerr << "Json Buffer: " << jsonBuffer << "\n";

			return jsonBuffer;
		}


		bool SensorDataHandler::isPreviousDataOld(struct dleft_table_entry * prev, struct dleft_table_entry * next)
		{
			return (prev->timestamp < (next->timestamp - OLD_DATA_TIMEOUT));
		}


		int SensorDataHandler::buildTableKey(struct ipv4 * data, HashedBuffer<KEY_BYTE_SIZE>& key)
		{
			key << data->length << data->protocol << data->source << data->destination;
			
			if(ntohs(data->protocol) == 6)
				return buildTableKey((struct tcp*) data, key);
			if(ntohs(data->protocol) == 17)
				return buildTableKey((struct udp*) data, key);
			
			return 0;
		}
		
		int SensorDataHandler::buildTableKey(struct tcp * data, HashedBuffer<KEY_BYTE_SIZE>& key)
		{
			
			key << data->source_port << data->dest_port << data->seq_no << data->ack_no 
				<< data->checksum;
				
			return 0;
		}
		
		int SensorDataHandler::buildTableKey(struct udp * data, HashedBuffer<KEY_BYTE_SIZE>& key)
		{
			
			key  << data->source_port << data->dest_port;
				
			return 0;
		}

		/*
		int SensorDataHandler::parsePacket( char * buffer, const int rcv_size, struct packet_data * structured_packet)
		{
	
			struct packet_data * net_data = (struct packet_data *) buffer;

			structured_packet->head.timestamp = ntohs(net_data->head.timestamp);

			structured_packet->head.onlyHeader = ntohs(net_data->head.onlyHeader);

			// structured_packet->head.dataLength = ntohs(net_data->head.dataLength);
			
			structured_packet->data;
			memcpy((void*)structured_packet->data, (void*)net_data->data, dataLength);

			return 0;
		}
		*/


}


