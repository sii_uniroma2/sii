#include <ConnectionListener.hpp>
#include <openssl/ssl.h>

namespace ssi
{

	ConnectionListener::ConnectionListener()
	{
		this->ssl_context = SSL_Context::getInstance().getRawContext();
		this->ssl_gate_listener = NULL;
	}
	
	
	uint64_t ConnectionListener::generateSensorID(const struct sockaddr_in *address)
	{
		uint32_t hash[2] = { 1, 2};
				 
		void * key = (void*) &(address->sin_port);
		size_t length = (sizeof(address->sin_port)) + (sizeof(address->sin_addr)); 
		
		hashlittle2(key, length, &hash[0], &hash[1]);
		
		uint64_t value = *(uint64_t*) hash;
		
		return value;
	}
	
	
	int ConnectionListener::set_gate(const char * ip_interface, const int port)
	{
		std::cout << " - - - ConnectionListener::set_gate(const char * ip_interface, const int port)\n";
		
		SSL_Gate * p_gate = new SSL_Gate( ssl_context);
		std::cout << " - - - - gate created\n";

		if(p_gate->set_gate(ip_interface, port) < 0 ) return -1;
		std::cout << " - - - - gate setted\n";

		this->ssl_gate_listener = p_gate;
		return 0;
	}



	int ConnectionListener::setConnectionInterface(const char * ip_interface, const int port)
	{
		std::cout << " - - ConnectionListener::setConnectionInterface(const char * ip_interface, const int port)\n";
		if(ip_interface == NULL || port == 0 || this->ssl_context == NULL)
			return -1;
		
		std::cout << " - - - correct params\n";
		
		if(set_gate(ip_interface, port) < 0)
		{
			return -1;
		}

		std::cout << " - - - gate is set\n";
		return 0;
	}

	int ConnectionListener::startListening(const char * ip_interface, const int port)
	{
		std::cout << " - ConnectionListener::startListening(const char * ip_interface, const int port)\n";
		if(setConnectionInterface(ip_interface, port) < 0)
			return -1;

		std::cout << " - - over setConnectionInterface(ip_interface, port)\n";
		return startListening();
	}

	int ConnectionListener::startListening()
	{
		std::cout << " - - ConnectionListener::startListening()\n";
		if(this->ssl_gate_listener == NULL )
			return -1;
			
		std::cout << " - - - about to start listening\n";
		this->start();
		std::cout << " - - - thread is running\n";
		
		return 0;
	}

	int ConnectionListener::stopListening()
	{
		return -1;
	}

	void ConnectionListener::run()
	{
		std::cout << " * running\n";
		const struct sockaddr_in * inaddr;
		ssi::SensorHandler * handler;
		
		SensorHandler_params params;
		
		while(true)
		{
			std::cout << " * looping\n";
			inaddr = 0;
			
			std::cout << " * listening\n";
			SSL_IChannel * channel = this->ssl_gate_listener->accept_channel();
			
			if(channel == 0)
			{
				std::cerr << (this->ssl_gate_listener->get_err());
				continue;
			}
			std::cout << " * accepting\n";
			
			inaddr = channel->get_sockaddr();
			
			params.channel = channel;
			params.sensorID = generateSensorID(inaddr);

			handler = new ssi::SensorHandler(&params);
			
			this->active_sensors[params.sensorID] = handler;

			handler->command_plugin_get();
			
			//handler->command_start();
		}
	}
	
	SensorHandler * ConnectionListener::getSensorHandler(uint64_t sensor_id)
	{
		auto it = this->active_sensors.find(sensor_id);

		return (it == this->active_sensors.end() ? NULL : it->second);
	}

	
	int ConnectionListener::dispatchCommand(uint64_t sensor_id,
											command_id commandId,
											struct sensor_default_reply* reply,
											void* data,
											size_t dataLen)
	{
		SensorHandler * handler = getSensorHandler(sensor_id);

		int res = -1;

		if (handler)
		{
			res = handler->dispatchCommand(commandId, reply, data, dataLen);

			if (res == -1)
			{
				std::cerr << "[ConnectionListener::dispatchCommand] Destroying Sensor Info @ " << sensor_id << "\n\n";

				delete handler;

				active_sensors.erase(sensor_id);
			}
		}
		else
		{
			std::cerr << "[ConnectionListener] SensorID not found during dispatchCommand.\n";
		}
		
		return res;
	}


	int ConnectionListener::queryAll( std::map<uint64_t, struct sensor_default_reply>& replyMap )
	{
		struct sensor_default_reply currentReply;

		std::vector< std::pair<uint64_t, SensorHandler*> > sensors_to_remove;

		for (auto& sensor : active_sensors)
		{
			uint64_t sensorId = sensor.first;
			SensorHandler* handler = sensor.second;

			// std::cout << "[queryAll] Found SensorId: " << sensorId << "\n";

			if ( handler->dispatchCommand(command_id::STATUS, &currentReply) < 0 )
			{
				std::cerr << "[queryAll] Something goes wrong with SensorId: " << sensorId << "\n";

				sensors_to_remove.push_back( sensor );
			}

			replyMap[sensorId] = currentReply;
		}


		removeSensors(sensors_to_remove); 

		return 0;
	}
};

