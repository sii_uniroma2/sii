
#include <CounterManager.hpp>
#include <Registers.hpp>


using namespace ssi;


int CounterManager::count = 0;

std::map<uint64_t, CounterPlugin*> CounterManager::available;
std::map<uint64_t, CounterPlugin*> CounterManager::active;


void CounterManager::add(CounterPlugin* plugin)
{
    uint64_t id = plugin->getId();

    if ( available.find(id) != available.end() )
    {
        throw std::runtime_error("[CounterManager::Add] Found duplicate ids!!!");
    }

    available[id] = plugin;

    count++;
}

bool CounterManager::enable(uint64_t id)
{
    CounterPlugin* cp = available[id];

	bool result = false;

	if (Registers::createContext(id, cp->required_fields()))
	{
		active[id] = cp;

		result = true;
	}

    return result;
}

bool CounterManager::disable(uint64_t id)
{
    return active.erase(id);
}

bool CounterManager::status(uint64_t id)
{
  return active.find(id) != active.end();
}


bool CounterManager::enableAll()
{
	for (auto& x : available)
	{
		if (!enable(x.first)) return false;
	}

	return true;
}

void CounterManager::update(const Packet* packet)
{
    for (auto& x : active)
    {
        x.second->update(packet);
    }
}

/*
int CounterManager::getFirstSlotOf(uint64_t id)
{
    auto it = active.find(id);

    if (it == active.end()) return -1;


    return (it->second)->startRegister;
}
*/


std::string CounterManager::infoToJson()
{
	size_t plugins_no = active.size();

	std::stringstream ss;

	ss << "[";

	if (plugins_no > 0)
	{
		auto it = active.begin();

		ss << (it->second)->infoToJson();

		it++;

		for (; it != active.end(); it++)
		{
			ss << "," << (it->second)->infoToJson();
		}
	}

	ss << "]";

	return ss.str();
}
