
#include <PluginManager.hpp>
#include <iostream>

using namespace ssi;


Plugin* PluginManager::load(const std::string& filename)
{
  void* handle = NULL;  
  plugin_getter instance_getter = NULL;


  handle = dlopen(filename.c_str(), RTLD_NOW);

  if (handle == NULL)
    throw std::runtime_error(dlerror());

  instance_getter = (plugin_getter) dlsym(handle, "getInstance");

  
  if (instance_getter == NULL)
    throw std::runtime_error(dlerror());


  return instance_getter();
}



void PluginManager::loadDirectory(const std::string& path)
{
  DIR* folder = NULL;
  struct dirent* entry = NULL;
  Plugin* plugin = NULL;

  folder = opendir(path.c_str());

  if (folder == NULL)
    throw std::runtime_error(strerror(errno));


  while ( (entry = readdir(folder)) != NULL )
  {
    if (entry->d_type == DT_REG)
    {
		std::string name(entry->d_name);

		if ( name.rfind("so") == std::string::npos) continue;

		plugin = load(path+"/"+entry->d_name);

		if (plugin->getType() == PLUGIN_COUNTER)
			CounterManager::add( (CounterPlugin*) plugin );
    }
  }

  
  closedir(folder);
}
