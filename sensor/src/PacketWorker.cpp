

#include <PacketWorker.hpp>
#include <CounterManager.hpp>
#include <SSL_Channel.hpp>
#include <bob.h>

#include <iostream>
#include <ConnectionManager.hpp>

using namespace ssi;


void PacketWorker::run()
{
  // Registers::resetContexts();
  
  const struct timespec wait_time = { 0, 5000000 };

  int i = 0;

  while(true)
  {
    Packet* pkt = NULL;

    while(!queue.try_pop(pkt))
    {
      // printf("processed %d elements\n", i);

      nanosleep(&wait_time, NULL);
    }

    i++;

    // packet extracted

    if (pkt == NULL) break;

    pkt->parse();

    // do other chain things

    CounterManager::update(pkt);
    
    // Export()

	if (exportData && filter.evaluate(pkt->getRegisters()) == 1 )
	{
		SSL_IChannel& channel = ConnectionManager::getInstance().getDataChannel();

		// create hash of packet to send (from 64+8 bit to end)
		Registers* regs = pkt->getRegisters();

		char regsBuffer[1024];

		uint32_t regsBytes = regs->serialize(regsBuffer, 1024, Packet::DefaultFields::NUMBER);

		// std::cout << "Regs Number: " << regsNumber << "\n";

		packet_data* pd = NULL;
		size_t size = pkt->getPacketData(&pd);

				
		// here we need to handle communications errors
    	channel.send_msg(regsBuffer, regsBytes);
    	channel.send_msg((char *)pd, size);
	}

    if (pool)
    {
		pool->release(pkt);     // delete pkt;
    }
  }

  std::cout << "Worker ends!\n";

	// stop communications

	ConnectionManager::getInstance().closeDataChannel();
}
