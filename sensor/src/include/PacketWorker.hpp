
#pragma once

#include <Packet.hpp>
#include <Worker.hpp>
#include <ObjectPool.hpp>
#include <BloomFilter.hpp>

#include <stdlib.h>

#include <SensorFilter.hpp>

namespace ssi
{

  class PacketWorker : public Worker<Packet*>
  {
    // BloomFilter<8, 20> filter;

    SensorFilter& filter;

    ObjectPool<Packet>* pool;

	bool exportData;

    virtual void run();

  public:

    PacketWorker() : filter( SensorFilter::getInstance() ), exportData(false)
    {
    }
    
    virtual void setPool(void* _pool=NULL)
    {
      pool = (ObjectPool<Packet>*)_pool;
    }

	void enableExport()
	{
		exportData = true;
	}


	void disableExport()
	{
		exportData = false;
	}

	virtual ~PacketWorker()
	{	
	}
  };

}
