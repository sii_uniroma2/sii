
#pragma once

/* This file contains the callbacks to handle
   C2 commands */

#include <CommandProtocol.hpp>

namespace ssi
{
	bool start_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
	bool stop_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
	bool status_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
	bool rules_set_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
	bool rules_get_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
	bool plugins_get_command(const sensor_command_args* inArgs, sensor_command_args* outArgs);
}
