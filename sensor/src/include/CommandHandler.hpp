
#pragma once

#include <Thread.hpp>
#include <SSL_Context.hpp>
#include <SSL_Gate.hpp>
#include <CommandProtocol.hpp>

#include <array>

#define MAX_NUMBER_COMMANDS 16

namespace ssi
{
	typedef bool(*command_handle)(const sensor_command_args* inArgs, sensor_command_args* outArgs);


	class CommandHandler : public Thread
	{
		std::array<command_handle, MAX_NUMBER_COMMANDS> commands;

		SSL_IChannel& cmdChannel;

		public:

		CommandHandler(SSL_IChannel& channel) : cmdChannel(channel)
		{
		}


		bool add(command_handle handle, uint16_t commandId);

		bool remove(uint16_t commandId);

/*
		void stop()
		{
			pthread_kill(pthread_self(), SIGUSR1);
		}
*/

		virtual ~CommandHandler()
		{
		}

		protected:

		command_handle get(uint16_t commandId) const
		{
			if (commandId >= commands.size()) return NULL;

			return commands[commandId];
		}

		void run();
	};
}
