
#pragma once

#include <CounterPlugin.hpp>
#include <Packet.hpp>
#include <map>

namespace ssi
{
  class CounterManager
  {
    static int count;

    static std::map<uint64_t, CounterPlugin*> available;
    static std::map<uint64_t, CounterPlugin*> active;

    CounterManager();
    CounterManager(const CounterManager&) = delete;

  public:

    static void add(CounterPlugin*);

    static bool enable(uint64_t id);
    static bool disable(uint64_t id);

    static bool status(uint64_t id);

	static bool enableAll();

    static void update(const Packet* packet);

    static int getFirstSlotOf(uint64_t id);

    static int getCount() { return count; }

	static std::string infoToJson();

  };
}
