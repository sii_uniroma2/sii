
#pragma once

#include <string>
#include <rapidjson/document.h>
#include <ostream>

class Config
{

	Config(const rapidjson::Document& doc) : Device( doc["Device"].GetString() ),
											 RemoteAddress( doc["RemoteAddress"].GetString() ),
											 CmdPort( doc["CmdPort"].GetUint() ),
											 CertAuth( doc["CertAuth"].GetString() ),
											 CertKey( doc["CertKey"].GetString() ),
											 CertPath( doc["CertPath"].GetString() )
	{
	}

public:

	const std::string Device;
	const std::string RemoteAddress;
	const uint16_t    CmdPort;
	const std::string CertAuth;
	const std::string CertKey;
	const std::string CertPath;
	

	static Config parse(const std::string& path);


	friend std::ostream& operator<<(std::ostream& stream, const Config& cfg)
	{
		stream << "Device: " << cfg.Device << "\n";
		stream << "RemoteAddress: " << cfg.RemoteAddress << "\n";
		stream << "CmdPort: " << cfg.CmdPort << "\n";
		stream << "CertAuth: " << cfg.CertAuth << "\n";
		stream << "CertKey: " << cfg.CertKey << "\n";
		stream << "CertPath: " << cfg.CertPath << "\n";

		return stream;
	}
};
