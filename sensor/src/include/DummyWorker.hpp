
#pragma once

#include <Packet.hpp>
#include <Worker.hpp>
#include <ObjectPool.hpp>
#include <BloomFilter.hpp>

#include <stdlib.h>

#include <SensorFilter.hpp>

namespace ssi
{

  class DummyWorker : public IWorker<Packet*>
  {
    // BloomFilter<8, 20> filter;

    SensorFilter& filter;

	bool exportData;

	virtual bool append(Packet*);

    virtual void run();

    virtual void setPool(void* pool);

  public:

    DummyWorker() : filter( SensorFilter::getInstance() ), exportData(false)
    {
    }

	/*    
    virtual void setPool(void* _pool=NULL)
    {
      pool = (ObjectPool<Packet>*)_pool;
    }
	*/

	void enableExport()
	{
		exportData = true;
	}


	void disableExport()
	{
		exportData = false;
	}

	virtual ~DummyWorker()
	{	
	}
  };

}
