
#pragma once

#include <Plugin.hpp>
#include <CounterManager.hpp>
#include <cstring>
#include <map>
#include <stdexcept>

#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <dlfcn.h>

namespace ssi
{
  class PluginManager
  {

    PluginManager();
    PluginManager(const PluginManager&) = delete;

  public:

    static Plugin* load(const std::string& filename);

    static void loadDirectory(const std::string& path);

  };
}
