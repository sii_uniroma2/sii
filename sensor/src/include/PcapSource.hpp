
#pragma once

#include <Thread.hpp>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <string>

#include <stdint.h>
#include <pcap.h>

#include <ObjectPool.hpp>
#include <IWorker.hpp>
#include <PacketWorker.hpp>
#include <atomic>

namespace ssi
{

  class PcapSource : public Thread
  {
    static const int MAX_PACKET_SIZE = 1518;

    // ObjectPool<Packet> pool;

    // const uint8_t* buffer;

    IWorker<Packet*>* worker;

    pcap_t* phandle;

    struct bpf_program fp;

    char errbuf[PCAP_ERRBUF_SIZE];

	std::atomic<bool> running;
    

  public:
    PcapSource(const char* _device, IWorker<Packet*>* _worker) : // pool(4096),
																 worker(_worker),
																 phandle(NULL),
																 running(false)
    {
        assert(_device && _worker);

        if (_device[0] == '/')
        {
            phandle = pcap_open_offline(_device, errbuf);
        }
        else
        {
            phandle = pcap_open_live(_device, MAX_PACKET_SIZE, 0, 0, errbuf);
        }

        if (phandle == NULL)
        {
            perror("PcapSource");
            exit(-1);
        }


        // PacketWorker* pw = static_cast<PacketWorker*>(worker);

        // worker->setPool(&pool);
    }


    virtual ~PcapSource()
    {
        if (phandle)
            pcap_close(phandle);
    }


    int filter(const std::string& fs);


	void stop()
	{
		running = false;
	}


  protected:
    virtual void run();

  };

}
