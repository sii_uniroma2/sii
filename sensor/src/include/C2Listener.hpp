/*
 * C2Listener.hpp
 *
 *  Created on: Jan 3, 2013
 *      Author: gabriele
 */

#ifndef C2LISTENER_HPP_
#define C2LISTENER_HPP_

#define DEF_SENSOR_BUFFSIZE 2048

/*
 *  	Protocollo:
 *
 *  	per qualsiasi azione il C2 invia due messaggi
 *
 *  		1- messaggio di comando
 *  		2- messaggio di dati
 *
 *  	il primo contiene il tipo di comando e la dimensione dei dati inviata
 * 		il secondo contiene i dati da gestire
 *
 */

#include <openssl/ssl.h>

class C2Listener
{
protected:

	SSL_CTX * ctx;
	SSL_Channel channel;
	unsigned int buff_max_size;

	int cmd_update_rules(const char * buff, int datasize)
	{
		// gestisce il comando

		return 0;
	}

	int cmd_notify_rules(const char * buff,int datasize)
	{
		// gestisce il comando

		return 0;
	}

	int cmd_clear_rules(const char * buff, int datasize)
	{
		// gestisce il comando

		return 0;
	}

public:

	C2Listener(SSL_CTX * ctx, unsigned int buffsize = DEF_SENSOR_BUFFSIZE)
	{
		this->ctx = ctx;
		this->channel = new SSL_Channel(this->ctx);
		this->buff_max_size = buffsize;
	}

	void setup(const char * host, short int port)
	{
		this->channel.set_channel(host, port);
		return;
	}

	int work()
	{
		// riceve il primo pacchetto, interpretando il tipo di istruzione
		// e richiamando la corrispondente operazione da eseguire

		flt_cmd_t cmd;
		int cmd_size = sizeof(flt_cmd_t);
		char buff[this->buff_max_size];
		int err;
		do
		{
			err = this->channel.recv_msg((char*)&cmd, cmd_size);
			if(err==-1)
			{
				perror("errore");
			}

			err = this->channel.recv_msg((char*)buff, this->buff_max_size);
			if(err==-1)
			{
				perror("errore");
			}
			if(err != cmd.data_size)
			{
				printf("dati ricevuti != dati dichiarati\n");
			}

			switch(cmd.op)
			{
			case flt_op_t::FLT_PROTO_UPD :
				err = this->cmd_update_rules((char *) buff, cmd.data_size);
				break;
			case flt_op_t::FLT_PROTO_NFY :
				err = this->cmd_notify_rules((char *) buff, cmd.data_size);
				break;
			case flt_op_t::FLT_PROTO_CLR :
				err = this->cmd_clear_rules((char *) buff, cmd.data_size);
				break;
			}

		}while(err>=0);

		return 0;
	}



};


#endif /* C2LISTENER_HPP_ */
