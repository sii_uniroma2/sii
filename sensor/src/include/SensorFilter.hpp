/*
 * SensorFilter.hpp
 *
 *  Created on: Jan 4, 2013
 *      Author: gabriele
 */

#ifndef SENSORFILTER_HPP_
#define SENSORFILTER_HPP_

#include <atomic>

#include "RuleContainer.hpp"


namespace ssi
{

    class SensorFilter
    {
		std::atomic<RuleContainer*> rules;

		std::atomic<RuleContainer*> old;


		SensorFilter() : rules(new RuleContainer), old(NULL)
		{
		}


		SensorFilter(const SensorFilter&) = delete;
		
		SensorFilter& operator=(const SensorFilter&) = delete;

        public:

		static SensorFilter& getInstance()
		{
			static SensorFilter instance;
			return instance;
		}


		const RuleContainer* getRules() const
		{
			return rules;
		}


		void update(RuleContainer* last)
		{
			old = rules.exchange(last);

			if (old)
			{
				delete old;

				old = NULL;
			}

			std::cout << "[SensorFilter] Rules updated: \n";

			rules.load()->print();
		}


		int evaluate(Registers* regs)
		{
			return rules.load()->evaluate(regs);
		}


		virtual ~SensorFilter()
		{
			delete rules;

			if (old) delete old;
		}
    };
}


#endif /* SENSORFILTER_HPP_ */
