
#pragma once

#include <SSL_Context.hpp>
#include <SSL_Channel.hpp>
#include <atomic>
#include <iostream>

namespace ssi
{
	class ConnectionManager
	{	
		SSL_Channel* cmdChannel;
		SSL_Channel* dataChannel;

		int cmdPort;
		int dataPort;

		std::string cmdAddress;
		std::string dataAddress;

		int retryTimeout;

		mutable std::atomic<bool> spinning;


		ConnectionManager() : cmdChannel(NULL),
							  dataChannel(NULL),
							  cmdPort(1234),
							  dataPort(1235),
							  retryTimeout(2), // seconds
							  spinning(false)
		{
		}

		SSL_Channel* getChannel(const std::string& ip, int port) const;

		public:


		static ConnectionManager& getInstance()
		{
			static ConnectionManager connMgr;

			return connMgr;
		}


		ConnectionManager(ConnectionManager& mgr) = delete;


		SSL_IChannel& getCommandChannel();

		SSL_IChannel& getDataChannel();

		const int getCommandPort() const
		{
			return cmdPort;
		};
		
		const int getDataPort() const
		{
			return dataPort;
		};

		void closeCommandChannel();
		void closeDataChannel();

		void closeAll();

		void setCommandPort(int _port, const std::string& ip = "127.0.0.1")
		{
			cmdPort = _port;
			cmdAddress = ip;
		}


		void setDataPort(int _port, const std::string& ip = "127.0.0.1")
		{
			dataPort = _port;
			dataAddress = ip;
		}


		void setRetryTimeout(int _retryTimeout)
		{
			retryTimeout = _retryTimeout;
		}


		virtual ~ConnectionManager()
		{
			if (cmdChannel)
			{
				delete cmdChannel;
			}

			if (dataChannel)
			{
				delete dataChannel;
			}
		}
	};
}
