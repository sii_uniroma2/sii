
#pragma once

#include <PcapSource.hpp>
#include <PluginManager.hpp>
#include <DummyWorker.hpp>
#include <MultiPacketWorker.hpp>
// #include <ObjectPool.hpp>
#include <ConnectionManager.hpp>
// #include <CommandHandler.hpp>
#include <Config.hpp>
#include <vector>
#include <ifaddrs.h>

namespace ssi
{
	class SensorConfig
	{
		static SensorConfig* instance;

		std::vector<std::string> local_ip_vector;

		DummyWorker worker;
		PcapSource sniffer;

		std::string RemoteAddress;

		// ConnectionManager& connMgr;

		SensorConfig(const Config& cfg) : sniffer(cfg.Device.c_str(), &worker)
		{
			RemoteAddress = cfg.RemoteAddress;

			// Init SSL Context

			SSL_Context& context = SSL_Context::getInstance();

			context.setCertificateAutority(cfg.CertAuth);
			context.setCertificateKeyPair(cfg.CertPath, cfg.CertKey);

			// Fetch local ip addresses
			
			getLocalIps(cfg.Device.c_str(), this->local_ip_vector);

			Registers::createContext(0, Packet::DefaultFields::NUMBER); // create default context with 6 fields
			
			PluginManager::loadDirectory("./plugins");

			std::cout << CounterManager::getCount() << " plugin(s) loaded.\n";

			CounterManager::enableAll();

			ConnectionManager::getInstance().setCommandPort(cfg.CmdPort, cfg.RemoteAddress);
		}

		SensorConfig(const SensorConfig&) = delete; // avoid copy constructor

		void  getLocalIps(const char* device, std::vector<std::string>& vector_ip);

		public:

		static SensorConfig& create(const std::string& configpath)
		{
			if (instance == NULL)
			{
				instance = new SensorConfig( Config::parse(configpath) );
			}

			return *instance;
		}

		static SensorConfig& getInstance()
		{
			if (instance == NULL)
			{
				throw std::runtime_error("[SensorConfig] You must create an instance first with create().");
			}

			return *instance;
		}


		static void destroy()
		{
			if (instance)
			{
				instance->stop();

				delete instance;

				instance = NULL;

				std::cout << "[SensorConfig] Destructed.";
			}
		}


		bool start(uint16_t port);

		bool stop();

		bool status() const
		{
			return sniffer.isRunning();
		}

		virtual ~SensorConfig()
		{
		}
	};
}
