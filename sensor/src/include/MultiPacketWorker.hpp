
#pragma once

#include <Packet.hpp>
#include <Worker.hpp>
#include <ObjectPool.hpp>

#include <stdlib.h>

namespace ssi
{

  template<unsigned W>
  class MultiPacketWorker : public IWorker<Packet*>
  {
    PacketWorker workers[W];

    virtual void run()
    {
      for (unsigned i=0; i < W; i++)
	workers[i].start();
    }

  public:
    virtual bool append(Packet* p)
    {
      static int i = 0;

      // printf("appending to worker %d\n", i);

      bool res = workers[i].append(p);

      i = (i+1)%W;

      return res;
    }


    virtual void setPool(void* _pool)
    {
      for (auto& x: workers)
      {
	x.setPool(_pool);
      }
    }
   

  };

}
