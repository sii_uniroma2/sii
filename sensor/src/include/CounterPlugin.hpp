
#pragma once

#include <Plugin.hpp>
#include <IBuffer.hpp>
#include <Packet.hpp>
// #include <CounterManager.hpp>

#include <vector>
#include <cassert>
#include <list>

namespace ssi
{
    class CounterPlugin : public Plugin
    {

        // int startRegister;
        // int requiredFields;

        RegisterType* currentRegisters;

		std::list<std::string> fields;
        
        protected:

		void register_field(const std::string& field)
		{
			fields.push_back(field);
		}

		/*        
        void set_required_fields(int reqFields)
        {
            assert(reqFields > 0);

            requiredFields = reqFields;
        }
		*/

        RegisterType& getRegister(uint32_t index)
        {
            assert( currentRegisters && index < fields.size());

            return currentRegisters[index];
        }
                
        public:

        CounterPlugin() : currentRegisters(NULL)
        {
        }

		int required_fields() const
		{
			return fields.size();
		}

        virtual enum plugin_type getType() const 
        {
            return PLUGIN_COUNTER;
        }


        double update(const Packet* packet)
        {
			// std::cerr << "[CounterPlugin::update] id: " << this->id << "\n";

            currentRegisters = packet->getRegisters()->getContext(this->id);

			// struct packet_data* pd = NULL;

			// size_t ip_len = packet->getPacketData(&pd, true) - sizeof(packet_data_header);

            return _update(packet);
        }


        virtual bool _update(const Packet* packet) = 0;


		std::string infoToJson() const
		{
			static const char* _temp = "{ \"plugin\" : \"%llu\", \"fields\" : [%s] }";

			char buffer[1024];

			// catenate field names

			std::string fields_s;

			size_t fields_no = fields.size();

			if (fields_no > 0)
			{
				std::stringstream ss;

				auto it = fields.begin();

				ss << '\"' << *it << '\"';

				it++;

				for (; it != fields.end(); it++)
				{
					ss << ", \"" << *it << '\"';
				}

				fields_s = ss.str();
			}
			
			int written = snprintf(buffer, 1024, _temp, this->id, fields_s.c_str());

			return std::string(buffer, written);
		}

    };

}
