
#pragma once

#include <algorithm>
#include <string>
#include <cstring>
#include <bob.h>

namespace ssi
{
  enum plugin_type
  {
    PLUGIN_SNIFFER,
    PLUGIN_COUNTER,
    PLUGIN_FILTER,
    PLUGIN_EXPORTER
  };


  class Plugin
  {

  protected:

    uint64_t id;

  public:

    // static Plugin* load(const char* filename);

    Plugin() : id(0) {}

    uint64_t getId()
	{
		if (id == 0)
		{
			const std::string& name = getName();

			size_t len = std::min(8U, name.size());

			memcpy(&id, name.c_str(), len);
		}

		return id;
	}

    virtual enum plugin_type getType() const = 0;

    virtual std::string getName() const = 0;

    virtual std::string getDescription() const { return ""; }

    virtual std::string getAuthor() const { return ""; }

  };


  typedef Plugin*(*plugin_getter)(void);



#define PLUGIN_REGISTER(handler_class)		\
						\
  extern "C" handler_class* getInstance()	\
  {						\
     static handler_class __instance;		\
     return &(__instance);			\
  }						\

}
