
#pragma once

#include <Protocols.hpp>
#include <Registers.hpp>
#include <iostream>
#include <vector>
#include <stdexcept>
#include <stdint.h>
#include <memory>

namespace ssi
{
  class Packet 
  {
    // friend class CounterPlugin;

  protected:

    size_t length;

    mutable Registers regs;

    //uint8_t buffer[MAX_IP_SIZE];

	struct packet_data ip_data;

    struct ipv4* ip_packet;

	mutable bool headParsed;

  public:
 
    enum DefaultFields
    {
        IP_PROTO_R,
        IP_SRC_R,
        IP_DST_R,
        SRC_PORT_R,
        DST_PORT_R,
        FLAGS_R,
        NUMBER      
    };


	const uint64_t timestamp_us;

    static const std::string ipToString(uint32_t ip);

 
    Packet(const uint8_t* _buffer, size_t _size, uint64_t _timestamp_us) : headParsed(false)
																		   , timestamp_us(_timestamp_us)
	{
        if (_size < IP_OFFSET || _size > MAX_ETH_SIZE )
        {
            char err[64];
            snprintf(err, 64, "Packet(): size error => %u\n", _size);

            throw std::runtime_error(err);
        }

		
        length = _size-IP_OFFSET; // Payload length

        memcpy(this->ip_data.data, _buffer+IP_OFFSET, length);
        ip_packet = (struct ipv4 *) this->ip_data.data;
        this->ip_data.head.timestamp = _timestamp_us;
        getHash192(this->ip_data.head.hash);
        
    };


    struct tcp* tcp() const
    {
        struct tcp* tcp_packet = NULL;
		
        if (ip_packet->protocol == 6 && length > sizeof(struct tcp))
        {
            tcp_packet = (struct tcp*) this->ip_data.data;
			
			if (!headParsed)
			{
				uint32_t flags = ntohs(tcp_packet->offset_flags) & 0x00FF;

            	regs[SRC_PORT_R] << ntohs(tcp_packet->source_port);
				regs[DST_PORT_R] << ntohs(tcp_packet->dest_port);

				regs[FLAGS_R] << flags; // tcp_packet->flags.value;

				headParsed = true;
			}
        }

        return tcp_packet;
    }


    struct udp* udp() const
    {
        struct udp* udp_packet = NULL;

        if (ip_packet->protocol == 17 && length > sizeof(struct udp))
        {
            udp_packet = (struct udp*) this->ip_data.data;

			if (!headParsed)
			{
            	regs[SRC_PORT_R] << ntohs(udp_packet->source_port);
				regs[DST_PORT_R] << ntohs(udp_packet->dest_port);

				headParsed = true;
			}
        }

        return udp_packet;
    }

	// modificare per packet_data
	void getHash192(uint32_t seed[6])
	{
		for (int i=0; i < 6; i++)
			seed[i] = i;

		hashlittle2(&(this->ip_packet->protocol), length-9, (seed), (seed+1));
		hashlittle2(&(this->ip_packet->protocol), length-9, (seed+2), (seed+3));
		hashlittle2(&(this->ip_packet->protocol), length-9, (seed+4), (seed+5));
	}

    Registers* getRegisters() const { return &regs; }


	size_t getPacketData(struct packet_data ** data, bool withPayload = false) const;


    void parse();


	std::string toString() const
	{
		std::stringstream ss;

		for (uint32_t i=0; i < DefaultFields::NUMBER; i++)
			ss << regs[i].GetString() << "\t";

		ss << "\n";

		return ss.str();
	}


	friend std::ostream& operator<<(std::ostream& stream, const Packet& pkt)
	{
		stream << pkt.toString();

		return stream;
	}
  };

}




