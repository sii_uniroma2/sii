

#include <iostream>
#include <PluginManager.hpp>
#include <ConnectionManager.hpp>
#include <CommandHandler.hpp>
#include <Commands.hpp>
#include <SensorConfig.hpp>
#include <signal.h>

using namespace ssi;

SensorConfig& sensor = SensorConfig::create("default.cfg");

ConnectionManager& connMgr = ConnectionManager::getInstance();

void signal_handler(int signo)
{
	// stop sniffer if running

	sensor.stop();

	// close all communications
	// terminate command handler

	try
	{
		connMgr.closeAll();
	}
	catch (std::runtime_error& err)
	{
	}
}

void install_signal_handler(void)
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));

	// filter signals

	// sigemptyset(&ss);
	// sigaddset(&ss, SIGINT);

	// sigprocmask(SIG_BLOCK, &ss, NULL);


	// set signal handler

	sa.sa_handler = signal_handler;

	if ( sigaction(SIGINT, &sa, NULL) == -1 )
	{
		std::cerr << "[install_signal_handler] Failed.\n";
		exit(-1);
	}
}


int main(void)
{
	install_signal_handler();
	
	// Try connect to control center

	SSL_IChannel& cmdChannel = connMgr.getCommandChannel();

	std::cout << "[SENSOR] Connected to Control Center.\n";

	// Init CommandHandler
 
	CommandHandler cmdh(cmdChannel);

	cmdh.add(start_command, command_id::START);
	cmdh.add(stop_command,  command_id::STOP);
	cmdh.add(status_command, command_id::STATUS);
	cmdh.add(rules_set_command, command_id::RULES_SET);
	cmdh.add(rules_get_command, command_id::RULES_GET);
	cmdh.add(plugins_get_command, command_id::PLUGIN_GET);

	cmdh.start();

	std::cout << "[SENSOR] Ready to receive commands.\n";

	// cmdh.join();

	pause();


	// freeing resources
	SensorConfig::destroy();

	std::cerr << "[SENSOR] Exiting... ";

    return 0;
}
