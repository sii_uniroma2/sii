
#include <Commands.hpp>
#include <SensorConfig.hpp>
#include <RuleContainer.hpp>
#include <SensorFilter.hpp>
#include <CounterManager.hpp>

using namespace ssi;

bool ssi::start_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	if (inArgs->length != sizeof(uint16_t)) return false;


	SensorConfig& sensor = SensorConfig::getInstance();

	uint16_t port = *reinterpret_cast<const uint16_t*>(inArgs->buffer);

	std::cout << "[SENSOR] Start command received.\n";
	
	bool res = sensor.start(port);

	// here wait for start before return

	while ( sensor.status() != true );

	return res;
}


bool ssi::stop_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	SensorConfig& sensor = SensorConfig::getInstance();

	std::cout << "[SENSOR] Stop command received.\n";

	return sensor.stop();
}

bool ssi::status_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	SensorConfig& sensor = SensorConfig::getInstance();

	std::cout << "[SENSOR] Status queried => " << sensor.status() << "\n";

	// this command cannot fail

	return true;
}


bool ssi::rules_set_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	RuleContainer* rc = new RuleContainer();

	if ( !RuleContainer::fromBuffer(inArgs->buffer, inArgs->length, rc) )
	{
		delete rc;

		return false;
	}

	SensorFilter::getInstance().update(rc);

	return true;
}


bool ssi::rules_get_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	int written = -1;

	const RuleContainer* rules = SensorFilter::getInstance().getRules();

	if (rules == NULL)
	{
		written = 1;

		*outArgs->buffer = 0;
	}
	else
	{
		written = rules->toBuffer(outArgs->buffer, RESPONSE_BUFFER_SIZE);

		if (written < 0)
		{
			return false;
		}
	}

	outArgs->length = (size_t) written;

	return true;
}


bool ssi::plugins_get_command(const sensor_command_args* inArgs, sensor_command_args* outArgs)
{
	std::string data = CounterManager::infoToJson();

	memcpy( outArgs->buffer, data.c_str(), data.size() );

	outArgs->length = data.size();

	return true;
}
