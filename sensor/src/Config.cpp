
#include <Config.hpp>
#include <iostream>
#include <fstream>

	/*
	const std::string Device;
	const std::string RemoteAddress;
	const uint16_t    CmdPort;
	const std::string CertAuth;
	const std::string CertKey;
	const std::string CertPath;
	*/
	

Config Config::parse(const std::string& path)
{
	char buffer[1024];

	memset(buffer, 0, 1024);

	std::ifstream input(path.c_str());

	input.read(buffer, 1024);

	size_t readLen = strlen(buffer);

	std::cout << "ReadLen: " << readLen << "\n";

	std::cout << buffer << "\n";

	rapidjson::Document doc;

	doc.Parse<0>(buffer);

	return Config(doc);
};


/*
int main(void)
{
	Config cfg = Config::parse("default.cfg");

	std::cout << cfg;

	return 0;
}
*/
