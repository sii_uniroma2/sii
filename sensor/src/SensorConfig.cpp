
#include <SensorConfig.hpp>
#include <sstream>

using namespace ssi;

SensorConfig* SensorConfig::instance = NULL;

bool SensorConfig::start(uint16_t port)
{
	if (sniffer.isRunning()) return false;

	ConnectionManager& connMgr = ConnectionManager::getInstance();

	std::cout << "[SENSOR] Init communications on port " << port << "\n";

	connMgr.setDataPort(port, RemoteAddress);

	connMgr.getDataChannel();
	
	std::cout << "[SENSOR] Sniffer starting... ";

	worker.enableExport();

	std::stringstream ss;
	unsigned int i=0;
	
	ss << "ip and ( ";
	
	while(i<local_ip_vector.size())
	{
		ss << " not ( host " << local_ip_vector[i] << " and ( port " << connMgr.getDataPort() << " or port " << connMgr.getCommandPort() <<"))";
		
		if(++i < local_ip_vector.size())
		{
			ss << " and ";
		}
	}
	ss << ")";
	
	printf("%s\n", ss.str().c_str());
	
	sniffer.filter(ss.str());

	sniffer.start(); // this will start also the bounded worker

	std::cout << "done.\n";

	return true;
}

void SensorConfig::getLocalIps(const char* device, std::vector<std::string>& vector_ip)
{
	struct ifaddrs* addr = NULL;

	if ( getifaddrs(&addr) == 0 )
	{
		struct ifaddrs* a;

		for ( a = addr; a != NULL; a = a->ifa_next )
		{
			struct sockaddr_in* sa = (struct sockaddr_in*) a->ifa_addr;

			if (sa->sin_family == AF_INET && strcmp(device, a->ifa_name) == 0)
			{
				vector_ip.push_back( inet_ntoa(sa->sin_addr) ); 
			}
		}
	
		freeifaddrs(addr);
	}

	if (vector_ip.size() == 0)
		throw std::runtime_error("Device not found!");	
}

bool SensorConfig::stop()
{
	// ConnectionManager& connMgr = ConnectionManager::getInstance();

	sniffer.stop();

	sniffer.join();

	// connMgr.closeDataChannel();

	return true;
}
