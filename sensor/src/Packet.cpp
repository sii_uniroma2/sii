
#include <Packet.hpp>

using namespace ssi;

#define DEBUG 0


const std::string Packet::ipToString(uint32_t ip)
{
  unsigned char* ip_s = (unsigned char*) &ip;

  char ip_f[16];

  snprintf(ip_f, 16, "%u.%u.%u.%u", ip_s[0], ip_s[1], ip_s[2], ip_s[3]);
    
  return std::string(ip_f);
}


void Packet::parse()
{
  if (length < IP_HEADER_LEN)
  {
    printf("Packet::parse(): packet less than IP HEADER SIZE => length: %u\n", length);
    return;
  }

  ip_packet = (struct ipv4*) this->ip_data.data;

  regs[IP_PROTO_R] << ip_packet->protocol;

  regs[IP_SRC_R].append(ntohl(ip_packet->source), buffer_type::IPv4);

  regs[IP_DST_R].append(ntohl(ip_packet->destination), buffer_type::IPv4);

  udp();
  tcp();

   
#if DEBUG
  printf("%u\t%s\t%s\t", 
	 ip_packet->protocol, 
	 ipToString(ip_packet->source).c_str(), 
	 ipToString(ip_packet->destination).c_str()
	 );
#endif

}


size_t Packet::getPacketData(struct packet_data ** data, bool withPayload) const
{
	uint32_t overhead = 0; // ip packet

	if ( withPayload )
	{
		overhead = length - sizeof(struct ipv4);
	}
	else
	{
		if ( udp() )
		{
			overhead = 8;
		}
		else
		if ( tcp() )
		{
			overhead = 20;
		}
	}

	*(data) = (struct packet_data*) &this->ip_data;	
			
	return sizeof(struct packet_data_header) + sizeof(struct ipv4) + overhead;
}
