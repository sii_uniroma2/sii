
#include <CounterPlugin.hpp>
#include <iostream>
#include <DLeftCounter.cpp>
#include <Thread_lock.cpp>
#include "../Packet.cpp"
#include <iomanip>

class SynCounter : public ssi::CounterPlugin
{
	DLeftCounter syn_count;
	DLeftCounter synack_count;

public:

    SynCounter() : syn_count(8, 20),
				   synack_count(8, 20)
    {
		register_field("syn_no");
		register_field("syn_ack_no");
		register_field("ratio");
    }

    virtual std::string getName() const { return "sync"; }

    virtual bool _update(const ssi::Packet* pkt)
    {
		double syn_no     = 0;
		double syn_ack_no = 0;
		double ratio      = 0;

		ssi::tcp* tcp_packet = pkt->tcp();

		if (tcp_packet)
		{
			ssi::Registers* regs = pkt->getRegisters();

			IReadBuffer& flags_r = regs->get(ssi::Packet::DefaultFields::FLAGS_R);

			uint32_t flags = flags_r.Get<uint32_t>();

			if (flags == 0x02) // SYN
			{
				IReadBuffer& key = regs->get(ssi::Packet::DefaultFields::IP_SRC_R);

				syn_no = syn_count.add(key);

				syn_ack_no = synack_count.get(key);
			}
			else
			if (flags == 0x12) // SYN-ACK
			{
				IReadBuffer& key = regs->get(ssi::Packet::DefaultFields::IP_DST_R);

				syn_no = syn_count.get(key);

				syn_ack_no = synack_count.add(key);
			}
			else
			{
				IReadBuffer& key = regs->get(ssi::Packet::DefaultFields::IP_SRC_R);

				syn_no = syn_count.get(key);
				syn_ack_no = synack_count.get(key);
			}
		}


		ratio = syn_ack_no/syn_no;

		getRegister(0) << syn_no;
		getRegister(1) << syn_ack_no;
		getRegister(2) << ratio;

		/*
		if (ratio > 0 && ratio < 0.5)
		{
			std::cerr << *pkt;
			std::cerr << "SynNo: " << syn_no << "\n";
			std::cerr << "SynAckNo: " << syn_ack_no << "\n";
			std::cerr << "Ratio: " << ratio << "\n\n";
		}
		*/

		// std::cout << "syn_no: " << getRegister(0).GetString() << "\n";
		// std::cout << "syn_ack_no: " << getRegister(1).GetString() << "\n";
		// std::cout << "ratio: " << getRegister(2).GetString() << "\n";

		return true;
  }
  

};


PLUGIN_REGISTER(SynCounter);

