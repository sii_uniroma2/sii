
#include <CounterPlugin.hpp>
#include <DnsHeader.hpp>
#include <DnsResponse.cpp>
#include "../Packet.cpp"
// #include <iostream>

class DnsParser : public ssi::CounterPlugin
{

public:

    DnsParser()
    {
		register_field("flags");
		register_field("name");
		register_field("ip");
    }

    virtual std::string getName() const { return "dns"; }


    virtual bool _update(const Packet* packet)
    {
		struct packet_data* pd = NULL;

		size_t ip_len = packet->getPacketData(&pd, true) - sizeof(packet_data_header);

		DnsHeader head = DnsHeader::parse(pd->data, ip_len);

		if (!!head)
		{
			// std::cerr << "[Dns::_update] Header good.\n";

			getRegister(0) << head.flags;

			DnsResponse resp = DnsResponse::parse(head);

			if (!!resp)
			{
				// std::cerr << "[Dns::_update] Response good, alias: " << resp.getAlias() << "\n";

				getRegister(1) << resp.getAlias();

				auto& ip_list = resp.getAddresses();

				if (ip_list.size() > 0)
				{
					// std::cerr << "[Dns::_update] IP[0]: " << ip_list[0] << "\n";
					getRegister(2).append(ip_list[0], buffer_type::IPv4);
				}
			}
		}

		return true;
	}
  

};


PLUGIN_REGISTER(DnsParser);

