
#include <ConnectionManager.hpp>
#include <iostream>

using namespace ssi;


SSL_Channel* ConnectionManager::getChannel(const std::string& ip, int port) const
{
	SSL_Context& context = SSL_Context::getInstance();

	SSL_Channel* channel = new SSL_Channel( context.getRawContext() );

	int retcode;

	spinning = true;

	while ( spinning && (retcode = channel->set_channel(ip.c_str(), port)) < 0 )
	{
		std::cerr << "[ConnectionManager] Connection failed, retry...\n";

		sleep(retryTimeout);
	}

	if (spinning == false) // it means that has interrupted
	{
		delete channel;
		channel = NULL;

		throw std::runtime_error("[ConnectionManager] Interrupted.");
	}

	spinning = false;

	return channel;
}


SSL_IChannel& ConnectionManager::getCommandChannel()
{
	if (cmdChannel == NULL)
	{
		cmdChannel = getChannel(cmdAddress, cmdPort);
	}

	return *cmdChannel;
}


SSL_IChannel& ConnectionManager::getDataChannel()
{
	if (dataChannel == NULL)
	{
		dataChannel = getChannel(dataAddress, dataPort);
	}

	return *dataChannel;
}


void ConnectionManager::closeCommandChannel()
{
	if (cmdChannel)
	{
		cmdChannel->shut_channel();

		delete cmdChannel;

		cmdChannel = NULL;
	}

	spinning = false;
}


void ConnectionManager::closeDataChannel()
{
	if (dataChannel)
	{
		dataChannel->shut_channel();

		delete dataChannel;

		dataChannel = NULL;
	}

	spinning = false;
}

void ConnectionManager::closeAll()
{
	// std::cout << "[ConnectionManager] Closing All...\n";

	closeCommandChannel();
	closeDataChannel();
}
