
#include <PcapSource.hpp>
#include <Packet.hpp>
#include <iostream>
#include <stdexcept>

using namespace ssi;


int PcapSource::filter(const std::string& fs)
{
  if ( pcap_compile(phandle, &fp, fs.c_str(), 1, PCAP_NETMASK_UNKNOWN) < 0)
  {
	throw std::runtime_error(pcap_geterr(phandle));
    // return -1;
  }
  else
  if ( pcap_setfilter(phandle, &fp) < 0 )
  {    
    throw std::runtime_error(pcap_geterr(phandle));
    // return -1;
  }

  return 0;
}


void PcapSource::run()
{
	worker->start();

	int i = 0;

	const uint8_t* buffer = NULL;
	struct pcap_pkthdr* header = NULL;

	int retcode = 0;

	running = true;

	while ( (retcode = pcap_next_ex(phandle, &header, &buffer)) != -2 && running )
	{
		if (retcode != 1) continue;

		if (header->len != header->caplen)
			printf("len (%u) and caplen (%u) differs\n", header->len, header->caplen);

		uint64_t timestamp_us = (header->ts.tv_sec * 1000000ULL + header->ts.tv_usec);

		// Packet* p = pool.get(buffer, header->caplen, timestamp_us); // new Packet(buffer, header.caplen); // 

		Packet p(buffer, header->caplen, timestamp_us);

		if (!worker->append(&p))
		{
			std::cout << "Something strange happens!\n";
			// pool.release(p);
		}

		i++;
	}

	if (running == false)
	{
		std::cerr << "[PcapSource] Interrupted, finising jobs...\n";
	}

	std::cout << "End capture (" << i << " packets).\n";

	running = false;

	// need to stop worker!

	worker->append(NULL);

	worker->join();

	std::cerr << "[PcapSource] Thread exit.\n";
}
