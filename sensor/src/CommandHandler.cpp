
#include <CommandHandler.hpp>
#include <SensorConfig.hpp>

using namespace ssi;


bool CommandHandler::add(command_handle handle, uint16_t commandId)
{
	if (commandId >= commands.size()) return false;

	commands[commandId] = handle;

	return true;
}


bool CommandHandler::remove(uint16_t commandId)
{
	if (commandId >= commands.size()) return false;

	commands[commandId] = NULL;

	return true;
}


void CommandHandler::run()
{
	char rawCommand[COMMAND_BUFFER_SIZE];
	char responseBuffer[RESPONSE_BUFFER_SIZE];

	struct sensor_command*       command  = reinterpret_cast<sensor_command*>(rawCommand);
	struct sensor_default_reply* response = reinterpret_cast<sensor_default_reply*>(responseBuffer);

	struct sensor_command_args inArgs  = { 0, command->args };
	struct sensor_command_args outArgs = { 0, response->args };

	int readBytes = 0;

	while( (readBytes = cmdChannel.recv_msg(rawCommand, COMMAND_BUFFER_SIZE)) > 0 )
	{
		response->reply = false;

		if ( readBytes >= (int) sizeof(sensor_command) )
		{
			command_handle handle = get(command->commandId);

			if (handle)
			{
				inArgs.length  = readBytes - sizeof(sensor_command);
				outArgs.length = 0;

				// execute callback

				response->reply = handle(&inArgs, &outArgs);
			}

			response->commandId = command->commandId;
		}

		response->sensorStatus = SensorConfig::getInstance().status();

		// std::cout << "[CommandHandler RESPONSE] " << response->toJson(0) << "\n\n";

		// TODO: handle send_msg possible fail

		if ( cmdChannel.send_msg( (char*) response, sizeof(sensor_default_reply) + outArgs.length) < 0)
		{
			std::cerr << "[CommandHandler] Send Response FAILED!\n";
		}
	}

	std::cerr << "[CommandHandler] Exit.\n";
}
