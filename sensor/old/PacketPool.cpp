
#include <PacketPool.hpp>

using namespace ssi;


Packet* PacketPool::get(const uint8_t* _buffer, int _size, int _slots)
{
  Packet* pkt = new (ptr) Packet(_buffer, _size, _slots);

  count++;

  if ( count == size ) // full
  {
    // expand

    std::cout << "Try to realloc...\n";

    auto memory = new uint8_t[block_size*sizeof(Packet)];

    arenas.push_back(memory);

    ptr = memory;

    memory_end = memory + block_size*sizeof(Packet);
	
    size *= 2;
  }
  else
  {
    ptr += sizeof(Packet);

    if (ptr == memory_end) // arena full ? try another
    {
      arena_id = (arena_id+1) % arenas.size();

      // std::cout << "Switch arena: " << arena_id << "\n";

      ptr = arenas[arena_id];

      memory_end = ptr + block_size*sizeof(Packet);
    }
  }

  return pkt;
}


void PacketPool::release(Packet* pkt)
{
  // pkt->~Packet();

  count--;
}


