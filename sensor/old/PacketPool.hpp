
#pragma once

#include <atomic>
#include <Packet.hpp>
#include <stdint.h>

namespace ssi
{
  class PacketPool
  {
    std::vector<uint8_t*> arenas;

    uint32_t arena_id;

    uint8_t* ptr;

    uint8_t* memory_end;

    std::atomic<uint32_t> count;

    size_t size;

    size_t block_size;

  public:

    PacketPool(size_t _block_size)
    {
      block_size = _block_size;

      auto memory = new uint8_t[ block_size*sizeof(Packet) ];

      arenas.push_back(memory);

      arena_id = 0;

      ptr = memory;

      memory_end = memory + block_size*sizeof(Packet);

      count = 0;

      size = block_size;
    }


    Packet* get(const uint8_t* _buffer, int _size, int _slots=5);

    void release(Packet* pkt);


    ~PacketPool()
    {
      std::cout << "Pool destructor\n";

      for (auto x : arenas)
      {
	delete[] x;
      }
    }

    
  };

}
