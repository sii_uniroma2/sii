
SENSORNAME=sense
CONTROLNAME=control

all: sensor control


sensor: commons
	@cd sensor; $(MAKE) all; #mv sensor ../$(SENSORNAME);

control: commons
	@cd c2; $(MAKE) all; #mv control ../$(CONTROLNAME);

commons:
	@cd common; $(MAKE) all;

clean:
	@cd sensor; $(MAKE) clean;
	@cd c2; $(MAKE) clean;
	@cd common; $(MAKE) clean;
	rm -f $(SENSORNAME)
	rm -f $(CONTROLNAME)

