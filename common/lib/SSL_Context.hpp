
#pragma once

#include <openssl/ssl.h>
#include <openssl/crypto.h>
#include <openssl/err.h>
#include <stdexcept>
#include <cassert>

class SSL_Context
{
	SSL_CTX* context;

	SSL_Context()
	{
		SSL_library_init();

		SSL_load_error_strings();

		context = SSL_CTX_new(TLSv1_method());

		assert(context);
	}

	
	public:

	SSL_Context(const SSL_Context&) = delete; // avoid copying
	SSL_Context& operator=(const SSL_Context&) = delete; // avoid assign


	static SSL_Context& getInstance()
	{
		static SSL_Context instance;

		return instance;
	}


	SSL_CTX* getRawContext() const
	{
		return context;
	}


	void setCertificateAutority(const std::string& path);

	void setCertificateKeyPair(const std::string& cert_path, const std::string& key_path);

	virtual ~SSL_Context()
	{
		SSL_CTX_free(context);

		ERR_remove_state(0);
	}
};

