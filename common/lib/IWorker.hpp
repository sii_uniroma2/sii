
#pragma once

#include <Thread.hpp>

namespace ssi
{

  template<typename T>
  class IWorker : public Thread
  {
  protected:

	// virtual bool pop(T& job) = 0;
 
  public:
    
    virtual bool append(T job) = 0;

    virtual void setPool(void* pool) = 0;
    
  };

}
