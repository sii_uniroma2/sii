/*
 * SSL_Socket.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: gabriele
 */


#include "SSL_Channel.hpp"
#include <iostream>

int SSL_Channel::set_channel(const char* host, short int port)
{
	if(this->connected)
	{
		set_err("Errore, il canale è già attivo");
		return -1;
	}
	int err = 0;
	// struct sockaddr_in server_addr;

	// Set up a TCP socket

	this->sock = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);

	//RETURN_ERR(this->sock, "socket");
	if(this->sock == -1)
	{
		set_err("Errore nella dichiarazione della socket");
		return -1;
	}

	// address is class member
    memset(&address, 0, sizeof(address));
    address.sin_family      = AF_INET;
    address.sin_port        = htons(port);       // Server Port number, def: DEF_SERV_PORT
    address.sin_addr.s_addr = inet_addr(host); // Server IP 		, def: "127.0.0.1"

    // Establish a TCP/IP connection to the SSL client

    err = connect(sock, (struct sockaddr*) &address, sizeof(address));

    //RETURN_ERR(err, "connect");
    if(err == -1)
	{
    	std::string e = "ERRORE: ";
    	perror("ERRORE in connessione");
    	set_err("Errore nella richiesta di connessione");
		return -1;
	}

	ssl = SSL_new (ctx);
	//RETURN_NULL(ssl);
	if(ssl == NULL)
	{
		std::string e = "ERRORE: ";
		e.append(ERR_reason_error_string(err));
		//set_err("Errore nella creazione della struttura SSL");
		set_err(e);
		return -1;
	}

	// Assign the socket into the SSL structure (SSL and socket without BIO)
	SSL_set_fd(ssl, sock);

	// Perform SSL Handshake on the SSL client
	err = SSL_connect(ssl);

	//RETURN_SSL(err);
	if(err == -1)
	{
		set_err("Errore nella creazione della struttura SSL");

		SSL_free(ssl);

		ssl = NULL;

		return -1;
	}
	// PARTE INFORMATIVA: stampa a schermo informazioni su server (non necessario)

	this->connected = true;

	return 0;
}

int SSL_Channel::shut_channel()
{

	if(!this->connected) return 0; // non connesso

	int err = 0;

	err = SSL_shutdown(ssl);
	//RETURN_SSL(err);
	if(err==-1)
	{
		set_err("Errore in chiusura connessione SSL");
		return -1;
	}

	// Terminate communication on a socket
	err = close(sock);

	//RETURN_ERR(err, "close");
	if(err == -1)
	{
		set_err("Errore in chiusura socket");
		return -1;
	}

	// Free the SSL structure
	SSL_free(ssl);

	ssl = NULL;

	this->connected = false;
	return 0;

}

int SSL_Channel::send_msg(const char * buf, int len)
{
	if(this->connected == false)
	{
		set_err("Errore, canale inattivo");
		return -1;
	}


	int wr=0;

	wr = SSL_write(ssl, buf, len);

	if(wr == -1)
	{
		set_err("errore in send");
		return -1;
	}

	return wr;
}


int SSL_Channel::recv_msg(char * buf, int len, int32_t timeoutSeconds)
{
	if(this->connected == false)
	{
		set_err("Errore, canale inattivo");
		return -1;
	}

	timeval _tv = { timeoutSeconds, 0 };

	timeval* tv = NULL;

	if (timeoutSeconds > 0)
	{
		tv = &(_tv);
	}

	fd_set descSet;

	FD_ZERO(&descSet);

	FD_SET(this->sock, &descSet);

	int sk = select(this->sock + 1, &descSet, NULL, NULL, tv);

	int rd = -1;

	if (sk == 0)
	{
		std::cerr << "Timeout elapsed.\n";

		rd = 0;
	}
	else
	{
		rd = SSL_read(ssl, buf, len);

		/*
		   if(rd == -1)
		   {
		   set_err("Errore in receive");
		   return -1;
		   }
		 */
	}

	return rd;
}
