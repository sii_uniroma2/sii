
#pragma once

#include <IBuffer.hpp>
#include <ISet.hpp>
#include <HashSplitter.hpp>
#include <bitset>
#include <cmath>

namespace ssi
{

  template <size_t K, size_t S>
  class BloomFilter : public ISet
  {
      const uint32_t SPLIT_SIZE;
      const uint32_t SLOT_SIZE;

      HashSplitter splitter;

      std::bitset<(1 << S)> memory;


  public:

    BloomFilter() : SPLIT_SIZE(S - log2(K)), SLOT_SIZE(1 << SPLIT_SIZE)
    {
    }

    
    virtual void insert(IReadBuffer& key)
    {
        Digest d = key.GetHash();

        splitter.set(d, 1);

        uint32_t bit = 0;
      
        for (size_t i=0; i < K && splitter.next(SPLIT_SIZE, &bit); i++)
        {
            try
            {
                memory.set( i*SLOT_SIZE + bit );
            }
            catch ( std::out_of_range& ex )
            {
                std::cout << "AT CYCLE " << i << "\n";
                std::cout << "EXCEPTION SETTING => " << bit << "\n";
                exit(-1);
            }
        }
    }

    virtual bool contains(IReadBuffer& key)
    {
        Digest d = key.GetHash();

        splitter.set(d, 1);

        uint32_t bit = 0;
      
        for (size_t i=0; i < K && splitter.next(SPLIT_SIZE, &bit); i++ )
        {
            if ( memory[ i*SLOT_SIZE + bit] == 0 ) return false;
        }

        return true;
    }

  };

}
