
#pragma once

#include "IBuffer.hpp"

class FHashedBuffer : public IReadBuffer 
{
	private :
		
	uint32_t * keybuffer;	
	
	void setDefautlValues()
	{
		Count = 24;
		Type = buffer_type::UNKNOWN;
	}
		
	public :
	
	FHashedBuffer()
	{
		setDefautlValues();
	}
	
	FHashedBuffer(uint32_t buffer[6]) : keybuffer(buffer)
	{
		setDefautlValues();
	}
	
    virtual const unsigned char* GetRawData() const
    {
    	return (unsigned char *) keybuffer;
    }
   
	virtual Digest GetHash() const
	{
		return Digest(keybuffer, 6);
	}

	void setFKey(uint32_t buffer[6])
	{
		this->keybuffer = buffer;
	}
	
};
