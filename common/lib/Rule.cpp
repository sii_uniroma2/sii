
#include "Rule.hpp"

using namespace ssi;

// TEMPLATE AND TABLES

const char* __json_template_stmt = R"(
{	"plugin" : "%llu",
	"field"  : "%s",
	"op"     : "%s",
	"value"  : "%s"
})";

std::string rule_statement::__opToString[FLT_OP_NUM] = \
{
	"false",
	"true",
	"==",
	"!=",
	">",
	">=",
	"<",
	"<=",
	"contains"
};

std::map<std::string, uint32_t> Rule::tokenMap = \
{
	// fields

	{ "proto",    0 },
	{ "ip_src",   1 },
	{ "ip_dst",   2 },
	{ "src_port", 3 },
	{ "dst_port", 4 },
	{ "flags",    5 }
};

std::map<std::string, uint8_t> Rule::opsMap = \
{
	// operators
	{ "==",       FLT_OP_EQU },
	{ "!=",       FLT_OP_NEQ },
	{ ">",        FLT_OP_GTH },
	{ ">=",       FLT_OP_GEQ },
	{ "<",        FLT_OP_STH },
	{ "<=",       FLT_OP_SEQ }
};



// FUNCTIONS

std::string rule_statement::toJson(const std::map<uint64_t, StringList>* additionalFields) const
{
	// buffer dimensions
	//
	// json syn  52 bytes MAX
	// plugin    20 bytes MAX for 64bit number - log(2^64) ~= 19.2
	// field     10 bytes MAX
	// operation 10 bytes MAX
	// value     BUFFER_SIZE_MAX bytes

	// big total 92 + BUFFER_SIZE_MAX


	char strBuffer[512];

	std::stringstream ss;
	std::string op_s;


	const StringList* fieldTransformTable = fieldToString();

	if (plugin != 0 && additionalFields)
	{
		// std::cerr << "[rule_statement::toJson] plugin " << plugin << ", additionalFields present.\n";

		// retrieve fields table

		auto it = additionalFields->find(plugin);

		if (it == additionalFields->end())
		{
			// plugin not found!

			return "";
		}

		fieldTransformTable = &(it->second);

		// compute plugin name

		const char* plugin_name = reinterpret_cast<const char*>(&plugin);

		/*
		char plugin_name[8];

		memcpy(plugin_name, (char*) &plugin, sizeof(uint64_t));
		*/

		/*
		char* ptr = plugin_name;

		while (*ptr)
		{
			ss << *ptr;

			ptr++;
		}
		*/

		ss << plugin_name << "."; // std::string( (char*) plugin, sizeof(uint64_t) ) << ".";
	}


	if (field < fieldTransformTable->size())
	{
		ss << fieldTransformTable->at(field);
	}
	else
	{
		return "";
	}

	if (operation < FLT_OP_NUM)
	{
		op_s = __opToString[operation];
	}
	else
	{
		return "";
	}

	snprintf(strBuffer, 512, __json_template_stmt, plugin,
												   ss.str().c_str(),
												   op_s.c_str(),
												   buffer.GetString().c_str() );

	return strBuffer;
}

std::vector<std::string> Rule::__getReverseTokenMap()
{
	std::vector<std::string> reverse;

	for (auto& fields : tokenMap)
	{
		reverse[fields.second] = fields.first;
	}

	return reverse;
}


std::string Rule::toJson(const std::map<uint64_t, StringList>* additionalFields) const
{
	std::stringstream ss;

	ss << "[";

	if (statementCount > 0)
	{
		ss << statementList[0].toJson(additionalFields);

		for (uint8_t i = 1; i < statementCount; i++)
		{
			std::string stmt = statementList[i].toJson(additionalFields);

			if (!stmt.empty())
			{
				ss << "," << stmt;
			}
		}
	}

	ss << "]";

	return ss.str();
}


bool Rule::fromBuffer(const void* buffer, size_t size, Rule* out)
{
	if ( out == NULL || size < sizeof(RuleHeader) )
	{
		return false;
	}


	const RuleHeader* head = reinterpret_cast<const RuleHeader*>(buffer);

	// paranoic check
 
	if ( size != head->getByteSize() )
	{
		return false;
	}

	// ok, seems good

	for (uint8_t i = 0; i < head->getCount(); i++)
	{
		out->addStatement( head->getStatement(i) );
	}

	return true;
}


bool Rule::fromJsonArray(const rapidjson::Value& statements,
						 Rule* out,
						 const std::map<uint64_t, StringList>* additionalFields)
{
	if (out == NULL) return false;

	// std::cerr << "[Rule::fromJsonArray] BEGIN\n";

	for (rapidjson::SizeType i = 0; i < statements.Size(); i++)
	{
		auto& obj = statements[i];

		const std::string& field_s = obj["field"].GetString();
		const std::string& op_s    = obj["op"].GetString();
		const std::string& value_s = obj["value"].GetString();

		struct rule_statement st;

		st.plugin = 0;

		auto it = tokenMap.find(field_s);

		if (it == tokenMap.end()) // not default field
		{
			if (additionalFields == NULL) return false;

			// 1) retreive plugin name from field and convert to plugin_id

			//    check if field_s is in plugin.field format
			size_t dot_index = field_s.find('.');

			if (dot_index >= sizeof(uint64_t) - 1)
			{
				return false;
			}

			memcpy( (char*) &st.plugin, field_s.c_str(), dot_index );

			// std::cout << "[Rule::fromJsonArray] found plugin_id: " << st.plugin << "\n";

			// 2) try to found the field in the corrispondent map

			auto fieldsListPtr = additionalFields->find(st.plugin);

			if (fieldsListPtr == additionalFields->end())
			{
				return false;
			}

			const StringList& fieldList = fieldsListPtr->second;

			std::string field_name = field_s.substr(dot_index+1);

			// std::cout << "[Rule::fromJsonArray] found field_name: " << field_name << "\n";

			bool found = false;

			for (size_t i=0; i < fieldList.size(); i++)
			{
				if (fieldList[i] == field_name)
				{
					found = true;

					st.field = i;

					break;
				}
			}

			if (!found) return false;
		}
		else
		{
			st.field = it->second;
		}

		st.operation = opsMap[op_s];

		// convert value based on number of dots found in string

		uint32_t dots_count = 0;

		size_t value_s_len = value_s.size();

		for (size_t i = 0; i < value_s_len; i++)
		{
			if (value_s[i] == '.') dots_count++;
		}

		char* endptr = NULL;

		if (dots_count == 0) // integer ?
		{
			uint32_t value = strtoul(value_s.c_str(), &endptr, 10);

			if (*endptr == '\0') st.buffer << value;
		}
		else
		if (dots_count == 1) // double ?
		{
			double value = strtod(value_s.c_str(), &endptr);

			if (*endptr == '\0') st.buffer << value;
		}
		else
		if (dots_count == 3 && value_s_len >= 7 && value_s_len < 16) // ipv4 ?
		{
			in_addr ipa;

			if ( inet_pton(AF_INET, value_s.c_str(), &ipa) )
			{
				st.buffer.append( ntohl(ipa.s_addr), buffer_type::IPv4 );
			}
		}

		if (st.buffer.GetLength() == 0) // NONE match, copy string as is
		{
			st.buffer << value_s;
		}

		/*
		std::cerr << "[Rule::fromJsonArray] Value in the buffer:\n";

		st.buffer.dump();
		*/

		// add statement to rule
		out->addStatement(st);

		// std::cout << st << "\n";

		// printf("field: %u, op: %u, buffer: %s\n", st.field, st.operation, st.buffer.GetString().c_str());
	}

	// std::cerr << "[Rule::fromJsonArray] END\n\n";

	return true;
}


int Rule::evaluate(Registers* regs) const
{
	for (uint8_t i=0; i < statementCount; i++)
	{
		auto& statement = statementList[i];

		// std::cout << "[Rule::evaluate] " << statement;

		RegisterType* context = regs->getContext(statement.plugin);

		// TODO: here we must handle the error! (?)
		if (context == NULL)
		{
			std::cerr << "[Rule::evaluate] Bad plugin_id!\n";
			continue;
		}

		RegisterType& pbuff = context[statement.field];

		/*
		if (statement.plugin != 0)
		{
			std::cerr << "[Rule::evaluate] for plugin: " << statement.plugin << "\n";
			std::cerr << "buffer: " << pbuff.GetString() << "\n";
		}
		*/


		// statement.buffer.dump();

		// pbuff.dump();

		bool match = false;

		switch(statement.operation)
		{
			case rule_op_t::FLT_OP_FLS :
				match = false;
				break;

			case rule_op_t::FLT_OP_TRU :
				match = true;
				break;

			case rule_op_t::FLT_OP_EQU :
				match = (pbuff == statement.buffer);
				break;

			case rule_op_t::FLT_OP_NEQ :
				match = (pbuff != statement.buffer);
				break;

			case rule_op_t::FLT_OP_GTH :
				match = (pbuff > statement.buffer);
				break;

			case rule_op_t::FLT_OP_GEQ :
				match = (pbuff >= statement.buffer);
				break;

			case rule_op_t::FLT_OP_STH :
				match = (pbuff < statement.buffer);
				break;

			case rule_op_t::FLT_OP_SEQ :
				match = (pbuff <= statement.buffer);
				break;

			case rule_op_t::FLT_OP_CNT :
				match = false;
				break;

			default :
				match = false;
				break;
		}

		if (match == false)
		{
			// statement failed, whole rule fails

			return 0;
		}
	}

	return ( this->header_only ? 1 : 2 );
}
