
#include <CommandProtocol.hpp>
#include <arpa/inet.h>

namespace ssi
{	

	static const char* _json_template_default_reply = R"(
	{	"sensorId" : "%Lu",
		"command" : "%s",
		"state" : "%s",
		"message" : "%s",
		"args" : %s
	})";
	
	std::string sensor_default_reply::toJson(uint64_t sensor_id, const std::string& more) const
	{
		
		char buffer[1024];
		
		std::string str_command, str_reply, str_status;
		
		
		switch((command_id) commandId)
		{
			case START		: str_command = "START"; 	 break;
			case STOP 		: str_command = "STOP"; 	 break;
			case PAUSE 		: str_command = "PAUSE"; 	 break;
			case RULES_GET	: str_command = "RULES GET"; break;
			case RULES_SET  : str_command = "RULES SET"; break;
			case STATUS 	: str_command = "STATUS"; 	 break;
			case PLUGIN_GET : str_command = "PLUGIN GET";break;
			default 		: str_command = "UNKNOWN";   break;
		}
		
		switch( (return_code) reply)
		{
			case OK            : str_reply = "Ok";					 break;
			case BAD_RESPONSE  : str_reply = "Bad response.";		 break;
			case BAD_ARGS      : str_reply = "Bad arguments."; 		 break;
			case REMOTE_ERROR  : str_reply = "Command failed."; 	 break;
			case COMM_ERROR    : str_reply = "Communication error."; break;
			case TIMEOUT       : str_reply = "Timeout.";             break;
			default            : str_reply = "Unknown.";             break;
		}

		switch( sensorStatus )
		{
			case 1 : str_status = "RUNNING"; break;
			case 0 : str_status = "STOPPED"; break;
		}

		int len = snprintf(buffer, 1024, _json_template_default_reply,
						sensor_id, 
						str_command.c_str(),	// command
						str_status.c_str(),		// status
						str_reply.c_str(),		// message
						more.c_str()            // args
				);

		return std::string(buffer, len);
	}

		
	void sensor_start::toNetFormat()
	{
		commandId = htons(commandId);
		port = htons(port);
	}


	void sensor_default_reply::toNetFormat()
	{
		commandId = htons(commandId);
		reply = htons(reply);
	}

	
	void sensor_start::toHostFormat()
	{
		commandId = ntohs(commandId);
		port = ntohs(port);
	}
	
	
	void sensor_default_reply::toHostFormat()
	{
		commandId = ntohs(commandId);
		reply = ntohs(reply);
	}	
}
