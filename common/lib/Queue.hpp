
#pragma once

#include <atomic>
#include <stdexcept>

namespace ssi
{

  template<typename T, unsigned S=1024>
  class Queue
  {
    T* queue;

    std::atomic<unsigned> first;
    std::atomic<unsigned> last;
    std::atomic<unsigned> count;

  public:
  
    Queue() : queue(new T[S]), first(0), last(0), count(0)
    {
    }

    bool append(T elem)
    {
      if (last == S-1)
      {
	// printf("queue full, dropping.\n");
	return false; // drop
      }

      /*
      unsigned index = last+1;

      if (index == S)
      {
	index = 0;
      }


      count++;
      */
      queue[last+1] = elem;
      
      // commit append
      // last.store(index);
      last++;

      return true;
    }


    T pop()
    {    
      if (first != last)
      {
	/*
	unsigned index = first+1;
	if (index == S)
	  index = 0;
	*/

	auto result = queue[first+1];

	// commit pop
	first++;
	// count--;
	// first.store(index);
      
	return result;
      }
      else
      {
	throw std::runtime_error("Queue is empty");
      }
    }


    bool empty() const
    {
      return first == last;
    }


    unsigned size() const
    {
      return count;
    }

    
    virtual ~Queue()
    {
      delete[] queue;
    }

  };

}
