
#include "Protocols.hpp"

using namespace ssi;

static const char* _json_template_ip = R"(
{	"Timestamp" : "%llu",
	"Sensor Id" : "%llu",
	"Version" : "%u",
	"Header Length" : "%u",
	"Packet Length" : "%u",
	"Packet Id" : "%u",
	"Flags" : "0x%02x",
	"Fragment Offset" : "%u",
	"Time To Live" : "%u",
	"Protocol Id" : "%u",
	"Checksum" : "%u",
	"Source" : "%s",
	"Destination" : "%s",
	"Payload" : %s,
	"Registers" : %s
})";


static const char* _json_template_udp = R"(
{	"Source Port" : "%u",
	"Destination Port" : "%u",
	"Length" : "%u",
	"Checksum" : "%u"
})";


static const char* _json_template_tcp = R"(
{	"Source Port" : "%u",
	"Destination Port" : "%u",
	"Sequence Number" : "%u",
	"Acknoledgement Number" : "%u",
	"Data Offset" : "%u",
	"Flags" : "0x%02x",
	"Window Size" : "%u",
	"Checksum" : "%u",
	"Urgent Pointer" : "%u"
})";

std::string ipv4::toJson(uint64_t timestamp, uint64_t sensor_id, const std::string& payload, const std::string& args) const
{
	char buffer[1024];

	uint16_t foffset = ntohs(flags_foffset);

	struct in_addr ip_src = { source };
	struct in_addr ip_dst = { destination };

	std::string src = inet_ntoa(ip_src);
	std::string dst = inet_ntoa(ip_dst);

	int len = snprintf(buffer, 1024, _json_template_ip,
									timestamp,
									sensor_id,
									(ver_hlen >> 4),
									(ver_hlen & 0x0F),
									ntohs(length),
									ntohs(id),
									(foffset >> 13),
									(foffset & 13),
									time_to_live,
									protocol,
									ntohs(checksum),
									src.c_str(),
									dst.c_str(),
									payload.c_str(),
									args.c_str()
			);

	return std::string(buffer, len);
}


std::string udp::toJson() const
{
	char buffer[256];

	int len = snprintf(buffer, 256, _json_template_udp,
									ntohs(source_port),
									ntohs(dest_port),
									ntohs(length),
									ntohs(checksum)
			);

	return std::string(buffer, len);

	// return ip.toJson(timestamp, payload);
}


std::string tcp::toJson() const
{
	char buffer[512];

	uint16_t of = ntohs(offset_flags);

	uint16_t flags = (of & 0x00FF);

	uint16_t data_offset = (of >> 12);

	int len = snprintf(buffer, 512, _json_template_tcp,
									ntohs(source_port),
									ntohs(dest_port),
									ntohl(seq_no),
									ntohl(ack_no),
									data_offset,
									flags,
									ntohs(window_size),
									ntohs(checksum),
									ntohs(urg_ptr)
			);

	return std::string(buffer, len);

	// return ip.toJson(timestamp, payload);
}

std::string packet_data::toJson(const std::string& regs, uint64_t sensor_id) const
{
	std::string payload = "{}";

	if (data[9] == 17)
	{
		const struct udp* up = reinterpret_cast<const struct udp*>(data);

		payload = up->toJson();
	}
	else
	if (data[9] == 6)
	{
		const struct tcp* tp = reinterpret_cast<const struct tcp*>(data);

		payload = tp->toJson();
	}

	const struct ipv4* ip = reinterpret_cast<const struct ipv4*>(data);

	return ip->toJson(head.timestamp, sensor_id, payload, regs);
}
