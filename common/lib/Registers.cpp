
#include <Registers.hpp>

using namespace ssi;


// size_t Registers::used_registers = 0;

// std::map<uint64_t, size_t> Registers::contextMap;


bool Registers::createContext(uint64_t context_id, uint32_t required_slots)
{
	size_t used_registers = fetchUsedRegs();

	if (used_registers + required_slots < NUM_OF_REGISTERS)
	{
		contextMap()[context_id] = used_registers;

		std::cout << "[Registers::createContext] id: " << context_id << ", offset: " << used_registers << "\n";

		fetchUsedRegs(required_slots);

		return true;
	}

	return false;
}


uint32_t Registers::serialize(char* buffer, size_t max_len, int from, int to)
{
	if (!buffer) return 0;

	uint32_t writtenBytes = 1;

	*buffer = (to-from);

	for (; from < to; from++)
	{
		writtenBytes += regs[from].serialize(buffer+writtenBytes, max_len-writtenBytes);
	}

	return writtenBytes;
}
