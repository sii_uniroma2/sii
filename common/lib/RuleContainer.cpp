
#include <RuleContainer.hpp>
#include <limits.h>
#include "rapidjson/document.h"

using namespace ssi;


bool RuleContainer::fromBuffer(const char* buffer, size_t size, RuleContainer* out)
{
	if ( out == NULL || size < 1 )
	{
		return false;
	}

	uint8_t ruleCount = *reinterpret_cast<const uint8_t*>(buffer);

	const char* ptr = buffer + 1;


	for (uint8_t i = 0; i < ruleCount; i++)
	{
		const RuleHeader* head = reinterpret_cast<const RuleHeader*>(ptr);

		Rule r;

		if ( Rule::fromBuffer( head, head->getByteSize(), &r ) )
		{
			out->add_rule(r);
		}

		ptr += head->getByteSize();
	}
	

	return true;
}


int RuleContainer::toBuffer(char* buffer, size_t max_size) const
{
	int written = 0;

	if (rule_list.size() > UCHAR_MAX || max_size < 1)
	{
		std::cerr << "[RuleContainer::toBuffer] List size > 256 or MAX_SIZE < 1\n";
		return -1;
	}

	uint8_t ruleCount = rule_list.size();

	*buffer = ruleCount;

	written++;

	for (auto& rule : rule_list)
	{
		size_t ruleSize = rule.getByteSize();

		if (written + ruleSize > max_size)
		{
			std::cerr << "[RuleContainer::toBuffer] Buffer full on write ";
			std::cerr << ruleSize << " bytes over a buffer (" << written << "/" << max_size << ")\n";
			return -1;
		}

		memcpy( buffer + written, &rule, ruleSize );

		written += rule.getByteSize();
	}

	return written;
}


bool RuleContainer::fromJsonArray(const rapidjson::Value& rules,
								  RuleContainer* out,
								  const std::map<uint64_t, StringList>* additionalFields)
{
	if (out == NULL) return false;

	// std::cerr << "[RuleContainer::fromJsonArray] BEGIN\n";

	for (rapidjson::SizeType i = 0; i < rules.Size(); i++)
	{
		Rule r;

		if ( Rule::fromJsonArray( rules[i], &r, additionalFields ) )
		{
			out->add_rule(r);
		}
	}

	// std::cerr << "[RuleContainer::fromJsonArray] END\n\n";

    return true;
}


std::string RuleContainer::toJson(const std::map<uint64_t, StringList>* additionalFields) const
{
	std::stringstream ss;

	ss << "[";

	auto it = rule_list.begin();

	if (it != rule_list.end())
	{
		ss << it->toJson(additionalFields);

		for (++it; it != rule_list.end(); it++)
		{
			ss << "," << it->toJson(additionalFields);
		}
	}

	ss << "]";

	return ss.str();
}


int RuleContainer::evaluate(Registers* regs) const
{
	int result = 0;

	for (auto& rule : rule_list)
	{
		result = rule.evaluate(regs);

		if (result > 0) break;
	}

	return result;
}
