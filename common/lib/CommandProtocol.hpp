

#ifndef COMMANDPROTOCOL_HPP_
#define COMMANDPROTOCOL_HPP_

#include <cstdint>
#include <stdlib.h>
#include <string>
#include <iostream>

#define COMMAND_BUFFER_SIZE  512
#define RESPONSE_BUFFER_SIZE 1024

namespace ssi
{

	enum command_id : uint16_t
	{
		UNKNOWN,
		STATUS,
		START, 
		STOP,
		PAUSE,
		RULES_GET,
		RULES_SET,
		PLUGIN_GET
	};


	enum return_code : uint16_t
	{
		OK = 1,
		BAD_RESPONSE,   // response not valid
		BAD_ARGS,       // syntax error
		REMOTE_ERROR,   // something happen on the other side (comm good)
		COMM_ERROR,     // unable to communicate with sensor
		TIMEOUT 
	};


	struct sensor_command_args
	{
		size_t length;
		char*  buffer;
	};	
	
	struct sensor_command
	{
		uint16_t commandId; 
		char     args[0];

		sensor_command(command_id cmd) : commandId(cmd)
		{
		}
	};
	
	struct sensor_start
	{
		uint16_t commandId;
		uint16_t port;
		
		sensor_start() : commandId(command_id::START) {};
		void toNetFormat();
		void toHostFormat();
	};


	struct sensor_default_reply
	{
		uint16_t commandId;
		uint16_t reply; // OK or FAIL
		uint16_t sensorStatus;
		char     args[0];
		
		std::string toJson(uint64_t sensor_id, const std::string& more = "[]") const;
		void toNetFormat();
		void toHostFormat();
	};


	struct sensor_rules_reply
	{
		struct sensor_default_reply reply;

		std::string toJson(uint64_t sensor_id) const;
	};
}

#endif
