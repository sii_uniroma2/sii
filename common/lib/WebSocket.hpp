
#pragma once

#include "websocketpp/config/asio_no_tls.hpp"
#include "websocketpp/server.hpp"
#include "Thread.hpp"
#include <mutex>

namespace ssi
{
	typedef websocketpp::server<websocketpp::config::asio> WsServer;
	typedef websocketpp::frame::opcode::value WsOpcode;

	typedef void(*wss_open_procedure)(void);
	typedef void(*wss_receive_procedure)(const std::string&);

/*
	class WebSocket
	{
		WsServer& server;
		websocketpp::connection_hdl handle;


		WebSocket(WsServer& _server, websocketpp::connection_hdl _handle) : 
																			server(_server),
																			handle(_handle)
		{
		}


	public:
		
		WebSocket getInstance(WsServer& _server, websocketpp::connection_hdl _handle)
		{
			if (_handle == NULL) throw std::runtime_error("[WebSocket] handle NULL!");

			return WebSocket(_server, _handle);
		}


		void send(const std::string& message)
		{
			server.send(handle, message, WsOpcode::text);
		}
	};
*/

	class WebSocketServer : public Thread
	{
		WsServer server;

		int port;

		static wss_open_procedure on_connect;
		static wss_receive_procedure on_message;

		static websocketpp::connection_hdl handle;

		// static std::mutex handle_mutex;

		
		static void _def_on_open(websocketpp::connection_hdl hdl)
		{
			if (handle.expired())
			{
				handle = hdl;

				if (on_connect) on_connect();
			}
		}


		static void _def_on_message(websocketpp::connection_hdl hdl, WsServer::message_ptr msg)
		{
			if (on_message) on_message(msg->get_payload());
		}


		static void _def_on_close(websocketpp::connection_hdl hdl)
		{
			std::cerr << "[WebSocket] Closed.\n";
		}


		static void _def_on_fail(websocketpp::connection_hdl hdl)
		{
			std::cerr << "[WebSocket] Failed for some reason.\n";
		}

	protected:

		virtual void run()
		{
			server.init_asio();
			server.listen(port);
			server.start_accept();
			server.run();
		}
	
	public:

		WebSocketServer(int _port) : port(_port)
		{
			server.set_open_handler(&_def_on_open);
			server.set_message_handler(&_def_on_message);
			server.set_close_handler(&_def_on_close);
			server.set_fail_handler(&_def_on_fail);
			
			std::cout << "[GuiWorker] Configured WebSocket server on port " << port << std::endl;
		}

		
		static void onConnect(wss_open_procedure handler);

		static void onMessageReceive(wss_receive_procedure handler);

		void send(const std::string& message)
		{
			if (!handle.expired())
			{
				server.send(handle, message, WsOpcode::text);
			}
		}


		virtual ~WebSocketServer() {}

	};
};
