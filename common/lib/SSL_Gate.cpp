/*
 * SSL_Gate.cpp
 *
 *  Created on: Dec 14, 2012
 *      Author: gabriele
 */

#include "./SSL_Gate.hpp"


SSL_IChannel* SSL_Gate::accept_channel()
{
	struct sockaddr_in cl_addr;
	unsigned int client_len = sizeof(cl_addr);

	int new_sock = -1;
	int err_code = -1;

	SSL*  ssl  = NULL;
	X509* cert = NULL;

	/* Socket for a TCP/IP connection is created */

	new_sock = accept(acc_sock, (struct sockaddr*)&cl_addr, &client_len);

	if (new_sock == -1)
	{
		set_err("Errore in listen");
		return NULL;
	}


	ssl = SSL_new(ctx);

	if (ssl == NULL)
	{
		set_err("Errore in creazione SSL");
		return NULL;
	}

	/* Assign the socket into the SSL structure (SSL and socket without BIO) */
	SSL_set_fd(ssl, new_sock);

	/* Perform SSL Handshake on the SSL server */
	err_code = SSL_accept(ssl);

	//RETURN_SSL(err);
	if (err_code == -1)
	{
		set_err("Errore in accettazione connessione SSL");
		goto err;
	}

	// NOW VERIFY CERTIFICATE

	cert = SSL_get_peer_certificate(ssl);

	if (!cert)
	{
		std::cout << "Certificate not found, drop.\n";

		goto err;
	}

	// verify cert!

	if (SSL_get_verify_result(ssl) != X509_V_OK)
	{
		std::cout << "Error in verify SSL cert.\n";

		goto err;
	}

	
	return new SSL_GateChannel(ssl, new_sock, &cl_addr);

err:

	SSL_free(ssl);
		
	return NULL;
}


SSL_IChannel* SSL_Gate::accept_channel(struct timeval timeout)
{
	struct sockaddr_in cl_addr;
	unsigned int client_len = sizeof(cl_addr);

	int new_sock = -1;
	int err_code = -1;

	SSL*  ssl  = NULL;
	X509* cert = NULL;

	/* Socket for a TCP/IP connection is created */

	fd_set rfds;
	int retval;

	FD_ZERO(&rfds);
	FD_SET(acc_sock, &rfds);

    retval = select(0, &rfds, NULL, NULL, &timeout);
	/* Don't rely on the value of tv now! */

   	if (retval == -1)
   	{
		perror("select()");
		return NULL;
	}
	else if(retval==0)
	{
		set_err("Timeout in accept");
		return NULL;
	}
	
	new_sock = accept(acc_sock, (struct sockaddr*)&cl_addr, &client_len);

	if (new_sock == -1)
	{
		set_err("Errore in listen");
		return NULL;
	}

	ssl = SSL_new(ctx);

	if (ssl == NULL)
	{
		set_err("Errore in creazione SSL");
		return NULL;
	}

	/* Assign the socket into the SSL structure (SSL and socket without BIO) */
	SSL_set_fd(ssl, new_sock);

	/* Perform SSL Handshake on the SSL server */
	err_code = SSL_accept(ssl);

	//RETURN_SSL(err);
	if (err_code == -1)
	{
		set_err("Errore in accettazione connessione SSL");
		goto err;
	}

	// NOW VERIFY CERTIFICATE

	cert = SSL_get_peer_certificate(ssl);

	if (!cert)
	{
		std::cout << "Certificate not found, drop.\n";

		goto err;
	}

	// verify cert!

	if (SSL_get_verify_result(ssl) != X509_V_OK)
	{
		std::cout << "Error in verify SSL cert.\n";

		goto err;
	}

	
	return new SSL_GateChannel(ssl, new_sock, &cl_addr);

err:

	SSL_free(ssl);
		
	return NULL;
}


int SSL_GateChannel::send_msg(const char * buf, int len)
{
	int wr=0;

	wr = SSL_write(ssl, buf, len);

	if(wr == -1)
	{
		set_err("Errore in send");
		return -1;
	}

	return wr;
}

int SSL_GateChannel::recv_msg(char * buf, int len, int32_t timeoutSeconds)
{
	int rd=0;

	rd = SSL_read(ssl, buf, len);

	if(rd == -1)
	{
		set_err("Errore in receive");
		return -1;
	}

	return rd;
}

int SSL_GateChannel::shut_channel()
{

	int err = 0;

	err = SSL_shutdown(this->ssl);
	//RETURN_SSL(err);
	if(err==-1)
	{
		set_err("Errore in chiusura connessione SSL");
		return -1;
	}

	// Terminate communication on a socket
	err = close(this->sock);

	//RETURN_ERR(err, "close");
	if(err == -1)
	{
		set_err("Errore in chiusura socket");
		return -1;
	}

	// Free the SSL structure
	SSL_free(this->ssl);

	this->ssl = NULL;

	return 0;
}


