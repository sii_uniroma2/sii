
/* ###########################################
 *
 * HASHED READBUFFER
 *
 * A simple wrapper around common data types
 * with support with cached hash
 *
 * NOTE: as not owned vars can be deleted,
 *       the user must care about this when
 *	     access to raw bytes
 *
 * 
 * Author: Giulio Picierro, CNIT
 *
 * Mail: giulio.picierro@uniroma2.it
 * 
 * Version: 1.0
 *
 * ##########################################
 */

#pragma once

#include <string>

#include <stdint.h>

#include "IBuffer.hpp"
#include "Digest.hpp"
#include "ReadBuffer.hpp"

#include <bob.h>
//#include <SpookyV2.h>

#define DIGEST_SIZE 9

class HashedReadBuffer : public ReadBuffer
{

    typedef ReadBuffer super;

protected:

    mutable uint32_t digest[DIGEST_SIZE];

    const Digest dig;


    void digest_reset() const
    {
        for (int i=0; i < DIGEST_SIZE; i++)
            digest[i] = i;
    }


public:

    HashedReadBuffer() : dig(digest, DIGEST_SIZE)
    {
		digest[0] = false;
    }

	template<typename T>
	HashedReadBuffer(const T& val) : ReadBuffer(val),
									 dig(digest, DIGEST_SIZE)
	{
		digest[0] = false;
	}


    Digest GetHash() const
    {
// #warning Remember that BIND does not reset hash, do MANUALLY with Reset()
        if (digest[0] != true)
        {
            digest_reset();

            // compute hash;

			// SpookyHash::Hash128( Data, Count, &digest[1], &digest[2]);
			// SpookyHash::Hash128( Data, Count, &digest[3], &digest[4]);

            hashlittle2( Data, Count, &digest[1], &digest[2] );
            hashlittle2( Data, Count, &digest[3], &digest[4] );
            hashlittle2( Data, Count, &digest[5], &digest[6] );
			hashlittle2( Data, Count, &digest[7], &digest[8] );

            digest[0] = true;
        }

        return dig;
    }

	void Reset()
	{
		digest[0] = false;

		super::Reset();
	}

};
