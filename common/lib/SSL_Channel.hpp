/*
 * SSL_Channel.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: gabriele
 */

#ifndef SSL_CHANNEL_HPP_
#define SSL_CHANNEL_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <cstdlib>

#ifdef __VMS
#include <types.h>
#include <socket.h>
#include <in.h>
#include <inet.h>

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif


#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include "common.hpp"

#include "SSL_IChannel.hpp"

class SSL_Channel : public SSL_IChannel {

protected:

	// std::string errmsg;
	SSL_CTX * ctx;
	// SSL * ssl;
	int sock;

	bool connected;


public:

	SSL_Channel(SSL_CTX *ctx) : SSL_IChannel()
	{
		this->ctx = ctx;
		// this->ssl = NULL;
		this->sock = 0;

		this->connected = false;
	}

	
	virtual ~SSL_Channel()
	{
	}

	/*
	int set_conn(const char* host = "127.0.0.1", short int port = DEF_SERVER_PORT);
	int shut_conn();
	*/

	int set_channel(const char* host = "127.0.0.1", short int port = DEF_SERVER_PORT);
	int shut_channel();

	virtual int send_msg(const char * buf, int len);
	virtual int recv_msg(char * buf, int len, int32_t timeoutSeconds = -1);

};



#endif /* SSL_SOCKET_HPP_ */
