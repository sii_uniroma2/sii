
#pragma once

namespace ssi
{
    class ISet
    {
        public:
    
        virtual bool contains(IReadBuffer& key) = 0;
        virtual void insert(IReadBuffer& key) = 0;
    };
}
