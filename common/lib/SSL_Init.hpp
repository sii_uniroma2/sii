/*
 * SSl_Init.hpp
 *
 *  Created on: Dec 13, 2012
 *      Author: gabriele
 */

#ifndef SSL_INIT_HPP_
#define SSL_INIT_HPP_

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include "common.hpp"

void FILTER_SSL_INIT();

SSL_CTX * CLIENT_SSL_CTX_NEW(int verify = DEF_VERIFY_CLIENT);
SSL_CTX * SERVER_SSL_CTX_NEW(int verify = DEF_VERIFY_SERVER);

#endif /* SSL_INIT_HPP_ */
