
#include "TewmaCounter.hpp"


double TewmaCounter::get(IReadBuffer& key, uint64_t timestamp)
{
    HashSplitter splitter( key.GetHash(), 1 );

    // time_t timestamp = (time_t) args;

    uint32_t H = 0;

    // get first splitted hash

    assert( splitter.next(shash, &H) );

    // now computes the old accumulated value and the new one before this insertion

	counterLock->lock(false);

    double minctr=c[H];

    double deltat;

    for(int i=1; i<=nhash; i++)
    {
		int64_t delta = timestamp - t[H];

		assert(delta >= 0);

		deltat = delta/W;

        // deltat = difftime(timestamp, t[H])/W;
        t[H] = timestamp;
        // assert(deltat>=0);

        c[H] *= pow(beta, deltat);
        if (c[H]<minctr) minctr=c[H];

        // get next splitted hash
        assert( splitter.next(shash, &H) );

        // printf("hash no => %d, value => %u\n", i, H(i));
    }

	counterLock->unlock();

    // finally return value. 
    return minctr;
}


double TewmaCounter::add(IReadBuffer& key, double quantity, uint64_t timestamp)
{
    HashSplitter splitter( key.GetHash(), 1 );

    // time_t timestamp = (time_t) args;

    uint32_t H[8];

    for (int i=1; i <= nhash; i++)
    {
        assert( splitter.next(shash, &H[i]) );
    }


	counterLock->lock(true);

    // computes the new counter decayed value
    double minctr=c[H[1]];

    double deltat;

    for(int i=1; i<=nhash; i++)
    {
		int64_t delta = timestamp - t[H[i]];

		assert( delta >= 0);

		deltat = delta/W;

        // deltat = difftime(timestamp, t[H[i]])/W;
        t[H[i]] = timestamp;
// #warning assert disabled
        // assert(deltat>=0);

        c[H[i]] *= pow(beta, deltat);
        if (c[H[i]]<minctr) minctr=c[H[i]];
    }

    // now compute the new value including the insertion and update 
    // the filter by waterfilling the new counter array bins
    double newctr=minctr+quantity*logbeta;
    assert(newctr<=1.0e99);

    for(int i=1; i<=nhash; i++) if (c[H[i]]<newctr) c[H[i]]=newctr;


	counterLock->unlock();
  
    // finally return value. 
    return newctr;

}

void TewmaCounter::clear()
{
    assert( t && c );

	counterLock->lock(true);

    memset(t, 0, slots * sizeof(uint64_t) );

    memset(c, 0, slots * sizeof(double) );

	counterLock->unlock();
}
