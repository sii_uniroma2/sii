/*
 * SSL_ChGate.hpp
 *
 *  Created on: Dec 14, 2012
 *      Author: gabriele
 */

#ifndef SSL_GATE_HPP_
#define SSL_GATE_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <cstdlib>

#ifdef __VMS
#include <types.h>
#include <socket.h>
#include <in.h>
#include <inet.h>

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "./openssl/crypto.h"
#include "./openssl/ssl.h"
#include "./openssl/err.h"
#include "./common.hpp"

#include "./SSL_IChannel.hpp"
#include <iostream>

class SSL_GateChannel : public SSL_IChannel
{
protected:

	int sock;

public:

	SSL_GateChannel(SSL * ssl, int sock, const struct sockaddr_in* addr)
	{
		this->ssl = ssl;
		this->sock = sock;

		// fill address structure

		if (addr)
		{
			memcpy(&address, addr, sizeof(struct sockaddr_in));
		}
	}

	virtual int send_msg(const char * buf, int len);

 	virtual int recv_msg(char * buf, int len, int32_t timeoutSeconds);

 	virtual int shut_channel();

	virtual ~SSL_GateChannel()
	{
	}
};


class SSL_Gate
{

protected:

	std::string errmsg;

	SSL_CTX * ctx;
	int acc_sock;

	void set_err(std::string s)
	{
		this->errmsg = s.append("\n");

		std::cerr << this->errmsg;
	}

public:

	SSL_Gate(SSL_CTX * ctx)
	{
		this->ctx = ctx;
		this->acc_sock = 0;
	}

	SSL_Gate(SSL_CTX * ctx, const char * i, int p)
	{
		this->ctx = ctx;
		this->acc_sock = 0;

		this->set_gate(i, p);
	}
	
	virtual ~SSL_Gate()
	{
		if(acc_sock!= 0)
			close(acc_sock);
	}

	const std::string get_err()
	{
		return this->errmsg;
	}

	int set_gate(const char * i, int port)
	{
		int err=0;

		acc_sock = socket(AF_INET, SOCK_STREAM, 0);
		if(acc_sock == -1)
		{
			set_err("Errore in creazione acc_socket");
			return -1;
		}

		struct sockaddr_in sa_addr;


	    memset((void*)&sa_addr, '\0', (unsigned long)sizeof(sa_addr));
	    sa_addr.sin_family      = AF_INET;
	    sa_addr.sin_port        = htons(port);       	// Server Port number, def: DEF_SERV_PORT
	    sa_addr.sin_addr.s_addr = inet_addr(i); 	// Server IP 		  , def: "127.0.0.1"

	    err = bind(acc_sock, (struct sockaddr*)&sa_addr, (unsigned int)sizeof(sa_addr));

	    if(err==-1)
	    {
	    	set_err("Errore in bind di ascolto");
	    	return -1;
	    }

		/* Wait for an incoming TCP connection. */
		err = listen(acc_sock, 5);

	    if(err==-1)
	    {
	    	set_err("Errore in listen");
	    	return -1;
	    }

		return 0;
	}

	SSL_IChannel * accept_channel();
	SSL_IChannel * accept_channel(struct timeval);

};


#endif /* SSL_CHGATE_HPP_ */
