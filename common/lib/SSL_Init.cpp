/*
 * SSL_Init.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: gabriele
 */

#include "SSL_Init.hpp"




void FILTER_SSL_INIT()
{
	SSL_library_init();

	/* Load the error strings for SSL & CRYPTO APIs */
	SSL_load_error_strings();
}

SSL_CTX * CLIENT_SSL_CTX_NEW(int verify)
{

	SSL_CTX * ctx = SSL_CTX_new(SSLv3_client_method());

	/* ------------------------------------------------------------- */
	/* 				    Sezione di setup verify-host				 */
	/* ------------------------------------------------------------- */

	if(verify == ON) {

		/* Blocco di caricamento del certificato client e relativa chiave privata.
		 * Al termine effettua un controllo per vedere se certificato e chiave corrispondono
		 */

		/* Load the client certificate into the SSL_CTX structure */
		if (SSL_CTX_use_certificate_file(ctx, RSA_CLIENT_CERT, SSL_FILETYPE_PEM) <= 0)
		{
			ERR_print_errors_fp(stderr);
            exit(1);
        }

		/* Load the private-key corresponding to the client certificate */
		if (SSL_CTX_use_PrivateKey_file(ctx, RSA_CLIENT_KEY,SSL_FILETYPE_PEM) <= 0)
		{
			ERR_print_errors_fp(stderr);
			exit(1);
		}

		/* Check if the client certificate and private-key matches */
		if (!SSL_CTX_check_private_key(ctx))
		{
			fprintf(stderr,"Private key does not match the certificate public key\n");
			exit(1);
        }
	}

	/* Load the RSA CA certificate into the SSL_CTX structure */
	/* This will allow this client to verify the server's     */
	/* certificate.                                           */
	if (!SSL_CTX_load_verify_locations(ctx, RSA_CLIENT_CA_CERT, NULL))
	{
		ERR_print_errors_fp(stderr);
		exit(1);
    }

	/* Set flag in context to require peer (server) certificate */
	/* verification */

	SSL_CTX_set_verify(ctx,SSL_VERIFY_PEER,NULL);
	SSL_CTX_set_verify_depth(ctx,1);

	return ctx;
}

SSL_CTX * SERVER_SSL_CTX_NEW(int verify)
{

	SSL_CTX * ctx = SSL_CTX_new(SSLv3_method());

	if (!ctx) {
		ERR_print_errors_fp(stderr);

		 exit(1);
	}

	/* ------------------------------------------------------------- */
	/* 				    Sezione di setup verify-host				 */
	/* ------------------------------------------------------------- */

	/* Load the server certificate into the SSL_CTX structure */
	if (SSL_CTX_use_certificate_file(ctx, RSA_SERVER_CERT, SSL_FILETYPE_PEM) <= 0) {

		ERR_print_errors_fp(stderr);
		exit(1);
	}

	/* Load the private-key corresponding to the server certificate */
	if (SSL_CTX_use_PrivateKey_file(ctx, RSA_SERVER_KEY, SSL_FILETYPE_PEM) <= 0) {

		ERR_print_errors_fp(stderr);
		exit(1);
	}

	/* Check if the server certificate and private-key matches */
	if (!SSL_CTX_check_private_key(ctx)) {

		fprintf(stderr,"Private key does not match the certificate public key\n");
		exit(1);
	}

	if(verify == ON) {

		/* Load the RSA CA certificate into the SSL_CTX structure */
		if (!SSL_CTX_load_verify_locations(ctx, RSA_SERVER_CA_CERT, NULL)) {

			ERR_print_errors_fp(stderr);
			exit(1);
		}

		/* Set to require peer (client) certificate verification */
		SSL_CTX_set_verify(ctx,SSL_VERIFY_PEER,NULL);

		/* Set the verification depth to 1 */
		SSL_CTX_set_verify_depth(ctx,1);

	}

	return ctx;
}
