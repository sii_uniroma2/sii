/*
 * common.cpp
 *
 *  Created on: Nov 15, 2012
 *      Author: gabriele
 */


#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>// for MAX_PATH
#include <iostream> // for cout and cin
#include "common.hpp"

int GetMaxPath()
{
	return PATH_MAX;
}

void GetCurrentPath(char* buffer)
{
	getcwd(buffer, PATH_MAX);
}


