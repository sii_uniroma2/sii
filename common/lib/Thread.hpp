

#pragma once

#include <cstdio>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <atomic>

namespace ssi
{

  class Thread
  {
    pthread_t my_thread;

    static void* __thread_handler( void* thread );

	std::atomic<bool> running;

  public:
    
    Thread() : my_thread(), running(false)
    {
    }

    Thread( const Thread& ) = delete; /* not copiable */

    void join();

    void start();

	bool isRunning() const { return running; }

  protected:

    virtual void run() = 0;

  };





}
