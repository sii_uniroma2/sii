
#pragma once
#include <cmath>

#include <IBuffer.hpp>
#include <HashSplitter.hpp>

template<size_t K, size_t S, size_t E>
class CountingBloomFilter
{
    const uint32_t SPLIT_SIZE;
    const uint32_t SLOT_SIZE;

    uint32_t memory[ log2(E)*(1<<S)/8 ];

    HashSplitter splitter;

    CountingBloomFilter() : SPLIT_SIZE(S - log2(K)), SLOT_SIZE(1 << SPLIT_SIZE)
    {
    }


    uint32_t increase(IReadBuffer& key, uint32_t value=1)
    {
        Digest d = key.GetHash();

        splitter.set(d, 1);
    }

    uint32_t count(IReadBuffer& key)
    {
        Digest d = key.GetHash();

        splitter.set(d, 1);

        
    }
};
