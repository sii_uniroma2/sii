
#include "WebSocket.hpp"

using namespace ssi;

wss_open_procedure WebSocketServer::on_connect = NULL;
wss_receive_procedure WebSocketServer::on_message = NULL;

websocketpp::connection_hdl WebSocketServer::handle;


void WebSocketServer::onConnect(wss_open_procedure handler)
{
	on_connect = handler;
}

void WebSocketServer::onMessageReceive(wss_receive_procedure handler)
{
	on_message = handler;
}
