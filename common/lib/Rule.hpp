/*
 * filter_rule.hpp
 *
 *  Created on: Dec 16, 2012
 *      Author: gabriele
 */

#ifndef FILTER_RULE_HPP_
#define FILTER_RULE_HPP_

#define BUFFER_SIZE_MAX     64
#define MAX_NUMBER_OF_STMT  64

#include <netinet/in.h>
#include <stdint.h>
#include <iostream>
#include <cstring>
#include <map>
#include <vector>
#include <string>
#include <iomanip>

#include "Registers.hpp"
#include "rapidjson/document.h"
#include "Buffer.cpp"


namespace ssi
{
	typedef std::vector<std::string> StringList;

	typedef enum : uint8_t
	{
		// operatori di debug
		FLT_OP_FLS, // FALSE
		FLT_OP_TRU, // TRUE

		// operatori per i filtri
		FLT_OP_EQU, // EQUAL
		FLT_OP_NEQ,	// NOT EQUAL
		FLT_OP_GTH, // GREATER THAN
		FLT_OP_GEQ, // GREATER OR EQUAL
		FLT_OP_STH, // SMALLER THAN
		FLT_OP_SEQ, // SMALLER OR EQUAL
		FLT_OP_CNT, // CONTAINS

		FLT_OP_NUM  // NUMBER OF OPERATORS

	} rule_op_t;


	struct rule_statement
	{
		static std::string __opToString[FLT_OP_NUM];

		uint64_t plugin;
		uint8_t field;
		uint8_t operation;

		Buffer<BUFFER_SIZE_MAX> buffer;

		static const std::vector<std::string>* fieldToString()
		{
			static const std::vector<std::string> __fieldToString = \
			{
				"proto",
				"ip_src",
				"ip_dst",
				"src_port",
				"dst_port",
				"flags"
			};

			return &(__fieldToString);
		}

		friend std::ostream& operator<<(std::ostream& stream, const rule_statement& stmt)
		{
			stream << "plugin: " << std::setbase(16) << stmt.plugin << " | field: " << uint16_t(stmt.field);

			std::string op = "not valid!";

			if (stmt.operation < FLT_OP_NUM)
			{
				op = __opToString[stmt.operation];
			}

			stream << " | op: " << op << " | value: " << stmt.buffer.GetString() << "\n\n";

			return stream;
		}

		std::string toJson(const std::map<uint64_t, StringList>* additionalFields = NULL) const;

	};


	class RuleHeader
	{
		protected:

			bool 	header_only;
			uint8_t statementCount;

			rule_statement statementList[0];

		public:

			RuleHeader(bool _headonly = 1) : header_only(_headonly),
											 statementCount(0)
			{
			}


			size_t getCount() const
			{
				return statementCount;
			}
			

			size_t getByteSize() const
			{
				return sizeof(RuleHeader) + sizeof(rule_statement)*statementCount;
			}


			const rule_statement& getStatement(uint8_t index) const
			{
				return statementList[index];
			}


			virtual ~RuleHeader()
			{
			}
	};


    class Rule : public RuleHeader
    {
		static std::map<std::string, uint32_t> tokenMap;
		static std::map<std::string, uint8_t>  opsMap;

		public:

			static std::vector<std::string> __getReverseTokenMap();

			static bool fromBuffer(const void* buffer, size_t size, Rule* out);
			static bool fromJsonArray(const rapidjson::Value& statements,
									  Rule* out,
									  const std::map<uint64_t, StringList>* additionalFields = NULL);


			// CONSTRUCTORS AND METHODS

            Rule(bool _header_only = 1) : RuleHeader(_header_only)
            {
            }


            bool addStatement(const rule_statement& st)
            {
				if ( statementCount >= MAX_NUMBER_OF_STMT ) return false;

				statementList[statementCount] = st;

				statementCount++;

				return true;
            }


            std::string toString() const
            {
				std::stringstream ss;

                for (uint8_t i = 0; i < statementCount; i++)
                {
					ss << statementList[i];
				}

				return ss.str();
			}


			std::string toJson(const std::map<uint64_t, StringList>* additionalFields = NULL) const;

			int evaluate(Registers* regs) const;

		private:

			rule_statement statementList[MAX_NUMBER_OF_STMT];



	    // Ritorna 0 se non ha match, 1 se match & richiesto solo l'header, 2 se match e richiesto tutto il pacchetto
			/*
            int evaluate(Packet * p)
            {
                 // Nota: L'operazione contains non è implementata, torna 0.

                for(std::vector<rule_statement>::iterator it = this->st_list.begin(); it!=this->st_list.end(); it++)
                {
                    int plugin_base_offset = CounterManager::getFirstSlotOf(it->plugin);

                    if (it->plugin == 0)
                    {
                        plugin_base_offset = 0;
                    }

                    if (plugin_base_offset == -1)
                    {
                        std::cout << "[filter_rule::evaluate()] Plugin ID " << it->plugin << " doesn't exists!\n";

                        return 0;
                    }

                    IReadBuffer& pbuff = p->getRegisters()->get(plugin_base_offset + it->field);
                    bool eval_check = 0;

                    switch(it->operation)
                    {

                        case rule_op_t::FLT_OP_FLS :
                            eval_check = 0;
                            break;

                        case rule_op_t::FLT_OP_TRU :
                            eval_check = 1;
                            break;

                        case rule_op_t::FLT_OP_EQU :
                            eval_check = (pbuff == it->buffer);
                            break;

                        case rule_op_t::FLT_OP_NEQ :
                            eval_check = (pbuff != it->buffer);
                            break;

                        case rule_op_t::FLT_OP_GTH :
                            eval_check = (pbuff > it->buffer);
                            break;

                        case rule_op_t::FLT_OP_GEQ :
                            eval_check = (pbuff >= it->buffer);
                            break;

                        case rule_op_t::FLT_OP_STH :
                            eval_check = (pbuff < it->buffer);
                            break;

                        case rule_op_t::FLT_OP_SEQ :
                            eval_check = (pbuff <= it->buffer);
                            break;

                        case rule_op_t::FLT_OP_CNT :
                            eval_check = 0;
                            break;

                        default :
                            eval_check = 0;
                    }
                    if(!eval_check) 
					{
						if(this->header_only)	{return 1;}
						else 			{return 2;}
					}
				}

                return true;
            }
			*/
    };
}




#endif /* FILTER_RULE_HPP_ */
