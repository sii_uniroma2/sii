
#pragma once

class DnsRecord
{

public:

    const std::string name;

    const class enum Type
    {
        A     = 1,
        NS    = 2,
        CNAME = 5,
        SOA   = 6,
        MB    = 7,
        MG    = 8,
        MR    = 9,
        NONE  = 10,
        WKS   = 11,
        PTR   = 12,
        HINFO = 13,
        MINFO = 14,
        MX    = 15,
        TXT   = 16,
        RP    = 17
        AFSDB = 18,
        X25   = 19,
        ISDN  = 20,
        RT    = 21,

        // NEW TYPES

        PX    = 26

    } rtype;


    class enum Class
    {
        IN = 1,
        CS = 2,
        CH = 3,
        HS = 4

    } rclass;


    const uint32_t ttl;

    const uint16_t length;

    const char* data;


    DnsRecord(std::string _name,
              uint16_t    _type,
              uint16_t    _class,
              uint16_t    _ttl,
              uint16_t    _len,
              char* _data = NULL ) : name(_name),
                                     rtype(_type),
                                     rclass(_class),
                                     ttl(_ttl),
                                     length(_len),
                                     data(_data)
    {
    }
};
