/*
 * common.hpp
 *
 *  Created on: Nov 15, 2012
 *      Author: gabriele
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_

#define ON      1
#define OFF     0

#define DEF_VERIFY_CLIENT ON
#define DEF_VERIFY_SERVER ON

#define VERBOSE OFF

#define DEF_SERVER_PORT 12348


#define RSA_CLIENT_CERT       "lib/cert/client.crt"
#define RSA_CLIENT_KEY  "lib/cert/client.key"
#define RSA_CLIENT_CA_CERT      "lib/cert/ca.crt"
#define RSA_CLIENT_CA_PATH		"lib/cert/"

#define RSA_SERVER_CERT     "lib/cert/client.crt"
#define RSA_SERVER_KEY          "lib/cert/client.key"
#define RSA_SERVER_CA_CERT "lib/cert/ca.crt"
#define RSA_SERVER_CA_PATH  "lib/cert/"

#define RETURN_NULL(x) if ((x)==NULL) exit(1)
#define RETURN_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }
#define RETURN_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(1); }

/*
enum RSA_ARTIFACT_ID
{
	RSA_NULL,
	RSA_SERVER_CA_CERT,
	RSA_SERVER_CERT,
	RSA_SERVER_KEY,
	RSA_CLIENT_CA_CERT,
	RSA_CLIENT_CERT,
	RSA_CLIENT_KEY
};
*/



int GetMaxPath();

void GetCurrentPath(char* buffer);
/*
void GetRSA(char * buffer, int rsa_id)
{
	char buf[GetMaxPath()];

	GetCurrentPath(buf);

	strapp();
	switch(rsa_id)
	{
		case RSA_SERVER_CA_CERT:

			break;
		case RSA_SERVER_CERT:

		case RSA_SERVER_KEY:
		case RSA_CLIENT_CA_CERT:
		case RSA_CLIENT_CERT:
		case RSA_CLIENT_KEY:

	}
}
*/
#endif /* COMMON_HPP_ */
