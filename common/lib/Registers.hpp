
#pragma once

#include <HashedBuffer.cpp>
#include <map>

#define NUM_OF_REGISTERS 16

namespace ssi
{
	typedef HashedBuffer<128> RegisterType;

    class Registers
    {
    
        RegisterType regs[NUM_OF_REGISTERS];

		static std::map<uint64_t, uint32_t>& contextMap()
		{
			static std::map<uint64_t, uint32_t> instance;

			return instance;
		}

		

	public:

		static bool createContext(uint64_t context_id, uint32_t required_slots = 1);

		static size_t fetchUsedRegs(uint32_t regs = 0)
		{	
			static size_t used_registers;

			used_registers += regs;

			return used_registers;
		}
		
		// static void resetContexts();

		RegisterType* getContext(uint64_t context_id)
		{
			auto& ctxMap = contextMap();

			auto it = ctxMap.find(context_id);

			return ( it != ctxMap.end() ? regs + it->second : NULL );
		}

        RegisterType& get(int index) { return regs[index]; }

        RegisterType& operator[](int index) { return regs[index]; }

		uint32_t serialize(char* buffer, size_t max_len, int from=0, int to=fetchUsedRegs());
    };

}
