
#ifndef _PROTOCOLS_H
#define _PROTOCOLS_H

#include <stdint.h>
#include <arpa/inet.h>
#include <iostream>
#include "Registers.hpp"

#define MAX_IP_SIZE 1500
#define MAX_ETH_SIZE 1518

#define IP_OFFSET 14
#define IP_HEADER_LEN 20
#define TCP_HEADER_LEN 40

namespace ssi
{

struct ipv4
{
    uint8_t ver_hlen;
    uint8_t _not_used;
    uint16_t length;
    uint16_t id;
    uint16_t flags_foffset;
    uint8_t time_to_live;
    uint8_t protocol;
    uint16_t checksum;
    uint32_t source;
    uint32_t destination;

    char payload[0];

	friend std::ostream& operator<<(std::ostream& stream, const struct ipv4& ip)
	{
		stream << "Version:\t" << (ip.ver_hlen >> 4) << "\n";
		stream << "Header Length:\t" << (ip.ver_hlen & 0x0F) << "\n";
		stream << "Packet length:\t" << ntohs(ip.length) << "\n";
		stream << "Packet Id:\t" << ntohs(ip.id) << "\n";

		uint16_t foffset = ntohs(ip.flags_foffset);

		stream << "Flags:\t" << (foffset >> 13) << "\n";

		stream << "Fragment offset:\t" << (foffset & 13) << "\n";
		stream << "Time to live:\t" << uint16_t(ip.time_to_live) << "\n";
		stream << "Protocol:\t" << uint16_t(ip.protocol) << "\n";

		stream << "Checksum:\t" << ntohs(ip.checksum) << "\n";
		stream << "Source:\t" << ntohl(ip.source) << "\n";
		stream << "Destination:\t" << ntohl(ip.destination) << "\n\n";

		return stream;
	}

	std::string toJson(uint64_t timestamp,
					   uint64_t sensor_id,
					   const std::string& payload = "{}",
					   const std::string& regs = "{}") const;
};


struct udp
{
    struct ipv4 ip;

    uint16_t source_port;
    uint16_t dest_port;
    uint16_t length;
    uint16_t checksum;

    char payload[0];

	std::string toJson() const;
};


struct tcp
{
	struct ipv4 ip;

    uint16_t source_port;
    uint16_t dest_port;
    uint32_t seq_no;
    uint32_t ack_no;
    uint16_t offset_flags;
    uint16_t window_size;
    uint16_t checksum;
    uint16_t urg_ptr;

	char payload[0];

	std::string toJson() const;
};



struct dns_header
{
    struct udp udp;

    uint16_t id;

    uint16_t flags;

    uint16_t total_questions;
    uint16_t total_answer_rr;
    uint16_t total_auth_rr;
    uint16_t total_additional_rr;

    uint8_t  data[0];
};


struct __attribute__((__packed__)) dns_record_attr
{    
    uint16_t type;
    uint16_t rclass;
    uint32_t ttl;
    uint16_t length;
    uint8_t  data[0];
};


struct packet_buffer
{
	uint8_t* buffer;
	const size_t   length;
	uint32_t hash[6];

	packet_buffer(size_t size) : buffer( new uint8_t[size] ),
								 length(size)
	{
	}

	virtual ~packet_buffer() { delete[] buffer; }
};


struct packet_data_header
{
	uint32_t hash[6];
	uint64_t timestamp;
	// uint16_t dataLength;
	// uint16_t onlyHeader;
};

struct packet_data
{
	struct packet_data_header head;
	
	uint8_t data[MAX_IP_SIZE];

	std::string toJson(const std::string& regs = "{}", uint64_t sensor_id = 0) const;
};

}; // end namespace ssi

#endif /* _PROTOCOLS_H */
