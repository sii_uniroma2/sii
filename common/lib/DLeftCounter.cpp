
#include "DLeftCounter.hpp"


double DLeftCounter::get(IReadBuffer& key, uint64_t args)
{
	counterLock->lock(false);

    auto entry = table->get(key);

    if (entry == NULL)
	{
		counterLock->unlock();

		return 0;
	}

	double res = *entry;

	counterLock->unlock();

    return res;
}


double DLeftCounter::add(IReadBuffer& key, double quantity, uint64_t args)
{
/*
	auto hash = key.GetHash().value()+1;
	auto size = key.GetHash().size()-1;

	std::cout << "[DLeftCounter::add] Hash: ";

	for (size_t i=0; i < size; i++)
	{
		std::cout << hash[i] << " ";
	}

	std::cout << "\n\n";
*/
	counterLock->lock(true);

    auto entry = table->get(key);

    if (entry == NULL)
    {
        entry = table->reserve(key);

        assert(entry);

        *entry = 0;
    }

    *entry += quantity;

	double res = *entry;

	counterLock->unlock();

    return res;
}

void DLeftCounter::clear()
{
	counterLock->lock(true);

    table->clear();

	counterLock->unlock();
}


bool DLeftCounter::reset(IReadBuffer& key)
{
	counterLock->lock(true);

	bool res = table->remove(key);

	counterLock->unlock();

    return res;
}
