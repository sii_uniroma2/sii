
#pragma once

#include <atomic>
#include <vector>
#include <iostream>

namespace ssi
{
  template<typename T>
  class ObjectPool
  {
    std::vector<uint8_t*> arenas;

    uint32_t arena_id;

    uint8_t* ptr;

    uint8_t* memory_end;

    std::atomic<uint32_t> count;

    size_t size;

    size_t block_size;

    void _new_arena()
    {
      auto memory = new uint8_t[ block_size*sizeof(T) ];

      arenas.push_back(memory);

      ptr = memory;

      memory_end = memory + block_size*sizeof(T);

      size += block_size;

      arena_id = arenas.size() - 1;
    }


  public:

    ObjectPool(size_t _block_size) : count(0), block_size(_block_size)
    {
      _new_arena();
    }


    template<typename ...Ti>
    T* get(Ti && ...arg)
    {
      T* obj = new (ptr) T(std::forward<Ti>(arg)...);

      count++;

      if ( count == size ) // full
      {
        // expand

        std::cout << "Try to realloc...\n";

        _new_arena();
      }
      else
      {
        ptr += sizeof(T);

        if (ptr == memory_end) // arena full ? try another
        {
          arena_id = (arena_id+1) % arenas.size();

          // std::cout << "Switch arena: " << arena_id << "\n";

          ptr = arenas[arena_id];

          memory_end = ptr + block_size*sizeof(T);
        }
      }

      return obj;
    }


    void release(T* obj)
    {
      obj->~T();

      count--;
    }


    virtual ~ObjectPool()
    {
      std::cout << "[Object Pool] Destructed\n";

      for (auto x : arenas)
      {
        delete[] x;
      }
    }
  };
}
