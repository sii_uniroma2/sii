/*
 * RuleContainer.hpp
 *
 *  Created on: Jan 4, 2013
 *      Author: gabriele
 */

#ifndef RULECONTAINER_HPP_
#define RULECONTAINER_HPP_

#include <list>
#include "Rule.hpp"

namespace ssi
{
    class RuleContainer
    {
        protected:

            std::vector<Rule> rule_list;

        public:

			static bool fromBuffer(const char* buffer, size_t size, RuleContainer* out);

			static bool fromJsonArray(const rapidjson::Value& rules,
									  RuleContainer* out,
									  const std::map<uint64_t, StringList>* additionalFields = NULL);


            virtual int add_rule(const Rule& r)
            {
                rule_list.push_back(r);

                return 0;
            }


			int toBuffer(char* buffer, size_t max_size) const;

			std::string toJson(const std::map<uint64_t, StringList>* additionalFields = NULL) const;


			void print() const
			{
				std::cout << "[RuleContainer::print] BEGIN\n";

				for (auto& rule : rule_list)
				{
					std::cout << rule.toString() << "\n";
				}

				std::cout << "[RuleContainer::print] END\n";
			}

			int evaluate(Registers* regs) const;


			virtual ~RuleContainer()
			{
			}
    };
}

#endif /* RULECONTAINER_HPP_ */
