
#include "SSL_Context.hpp"

// using namespace ssi;


// SSL_CTX* SSL_Context::context = NULL;


void SSL_Context::setCertificateAutority(const std::string& path)
{
	if (!SSL_CTX_load_verify_locations(context, path.c_str(), NULL))
	{
		throw std::runtime_error("Error loading CA cert.");
	}

	SSL_CTX_set_verify(context, SSL_VERIFY_PEER, NULL);
	SSL_CTX_set_verify_depth(context, 1);

	return;
}


void SSL_Context::setCertificateKeyPair(const std::string& cert_path, const std::string& key_path)
{
	if (SSL_CTX_use_certificate_chain_file(context, cert_path.c_str()) != 1)
	{
		throw std::runtime_error("Error loading certificate");
	}


	if (SSL_CTX_use_PrivateKey_file(context, key_path.c_str(), SSL_FILETYPE_PEM) != 1)
	{
		throw std::runtime_error("Error loading private key");
	}


	if (SSL_CTX_check_private_key(context) != 1)
	{
		throw std::runtime_error("Error check certificate-key pair");
	}

	return;
}

