
#pragma once

#include <IWorker.hpp>
#include <tbb/concurrent_queue.h>

namespace ssi
{
  template<typename T>
  class Worker : public IWorker<T>
  {

  protected:

    tbb::strict_ppl::concurrent_queue<T> queue;

	const struct timespec wait_time;


	virtual bool pop(T& element)
	{
		if (!queue.try_pop(element))
		{
			nanosleep(&wait_time, NULL);
		}

		return true;
	}

  public:

	Worker() : wait_time( {0, 5000000 } )
	{
	}

    size_t length() const { return queue.size(); }

    
    virtual bool append(T job)
    {
      queue.push(job);

      return true;
    }


	virtual ~Worker() {}
  };

}
