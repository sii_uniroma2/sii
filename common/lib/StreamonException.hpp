
#pragma once

#include <exception>
#include <string>
#include <sstream>
#include <execinfo.h>

#define MAX_ADDRESSES 32

class StreamonError
{
	// char** strings;

public:

	static std::string stacktrace()
	{
		std::stringstream ss;

		void* addrs[MAX_ADDRESSES];

		int addrs_no = 0;
		
		char** strings = NULL;

		addrs_no = backtrace( addrs, MAX_ADDRESSES );

		strings = backtrace_symbols(addrs, addrs_no);

		for (int i=0; i < addrs_no; i++)
		{
			ss << strings[i] << "\n";
		}

		return ss.str();
	}
};
