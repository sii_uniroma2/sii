
#pragma once

#include "IBuffer.hpp"

template<typename V>
class IDictionary
{

public:

    virtual V* add(const IReadBuffer& key, const V value) = 0;

    virtual V* get(const IReadBuffer& key) const = 0;

    virtual bool remove(const IReadBuffer& key) = 0;

    virtual void clear() = 0;

    virtual ~IDictionary()
    {
    }
 

};
