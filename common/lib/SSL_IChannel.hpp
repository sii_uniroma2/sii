/*
 * SSL_IChannel.hpp
 *
 *  Created on: Dec 14, 2012
 *      Author: gabriele
 */

#ifndef SSL_ICHANNEL_HPP_
#define SSL_ICHANNEL_HPP_


#include <string>
#include <openssl/ssl.h>
#include <openssl/crypto.h>
#include <openssl/err.h>

class SSL_IChannel
{
protected:

	SSL * ssl;
	std::string errmsg;

	struct sockaddr_in address;

	virtual void set_err(std::string s)
	{
		this->errmsg = s.append("\n");
		return;
	}

public:

	SSL_IChannel() : ssl(NULL) {}


	virtual ~SSL_IChannel()
	{
		if(this->ssl != NULL)
		{
			SSL_free(this->ssl);
		}
	}

	virtual int send_msg(const char * buf, int len) = 0;

	virtual int recv_msg(char * buf, int len, int32_t timeoutSeconds = -1) = 0;

 	virtual int shut_channel() = 0;

	virtual const std::string get_err()
	{
		return this->errmsg;
	}

	const struct sockaddr_in* get_sockaddr() const
	{
		return &address;
	}
};

#endif /* SSL_ICHANNEL_HPP_ */
