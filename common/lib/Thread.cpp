
#include "Thread.hpp"


using namespace ssi;


void* Thread:: __thread_handler( void* thread )
{
	Thread* t = static_cast<Thread*>(thread);

	t->running = true;

	t->run();

	t->running = false;

	return NULL;
}


void Thread::join()
{
	if (my_thread)
		pthread_join(my_thread, NULL);
}


void Thread::start()
{
	if (running) return;
	
	if ( pthread_create(&my_thread, NULL, __thread_handler, this) < 0)
	{
		perror("Thread: ");
	}
}
